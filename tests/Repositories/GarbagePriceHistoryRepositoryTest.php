<?php namespace Tests\Repositories;

use App\Models\GarbagePriceHistory;
use App\Repositories\GarbagePriceHistoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class GarbagePriceHistoryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var GarbagePriceHistoryRepository
     */
    protected $garbagePriceHistoryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->garbagePriceHistoryRepo = \App::make(GarbagePriceHistoryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_garbage_price_history()
    {
        $garbagePriceHistory = factory(GarbagePriceHistory::class)->make()->toArray();

        $createdGarbagePriceHistory = $this->garbagePriceHistoryRepo->create($garbagePriceHistory);

        $createdGarbagePriceHistory = $createdGarbagePriceHistory->toArray();
        $this->assertArrayHasKey('id', $createdGarbagePriceHistory);
        $this->assertNotNull($createdGarbagePriceHistory['id'], 'Created GarbagePriceHistory must have id specified');
        $this->assertNotNull(GarbagePriceHistory::find($createdGarbagePriceHistory['id']), 'GarbagePriceHistory with given id must be in DB');
        $this->assertModelData($garbagePriceHistory, $createdGarbagePriceHistory);
    }

    /**
     * @test read
     */
    public function test_read_garbage_price_history()
    {
        $garbagePriceHistory = factory(GarbagePriceHistory::class)->create();

        $dbGarbagePriceHistory = $this->garbagePriceHistoryRepo->find($garbagePriceHistory->id);

        $dbGarbagePriceHistory = $dbGarbagePriceHistory->toArray();
        $this->assertModelData($garbagePriceHistory->toArray(), $dbGarbagePriceHistory);
    }

    /**
     * @test update
     */
    public function test_update_garbage_price_history()
    {
        $garbagePriceHistory = factory(GarbagePriceHistory::class)->create();
        $fakeGarbagePriceHistory = factory(GarbagePriceHistory::class)->make()->toArray();

        $updatedGarbagePriceHistory = $this->garbagePriceHistoryRepo->update($fakeGarbagePriceHistory, $garbagePriceHistory->id);

        $this->assertModelData($fakeGarbagePriceHistory, $updatedGarbagePriceHistory->toArray());
        $dbGarbagePriceHistory = $this->garbagePriceHistoryRepo->find($garbagePriceHistory->id);
        $this->assertModelData($fakeGarbagePriceHistory, $dbGarbagePriceHistory->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_garbage_price_history()
    {
        $garbagePriceHistory = factory(GarbagePriceHistory::class)->create();

        $resp = $this->garbagePriceHistoryRepo->delete($garbagePriceHistory->id);

        $this->assertTrue($resp);
        $this->assertNull(GarbagePriceHistory::find($garbagePriceHistory->id), 'GarbagePriceHistory should not exist in DB');
    }
}
