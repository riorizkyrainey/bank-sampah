<?php namespace Tests\Repositories;

use App\Models\GarbageTypes;
use App\Repositories\GarbageTypesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class GarbageTypesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var GarbageTypesRepository
     */
    protected $garbageTypesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->garbageTypesRepo = \App::make(GarbageTypesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_garbage_types()
    {
        $garbageTypes = factory(GarbageTypes::class)->make()->toArray();

        $createdGarbageTypes = $this->garbageTypesRepo->create($garbageTypes);

        $createdGarbageTypes = $createdGarbageTypes->toArray();
        $this->assertArrayHasKey('id', $createdGarbageTypes);
        $this->assertNotNull($createdGarbageTypes['id'], 'Created GarbageTypes must have id specified');
        $this->assertNotNull(GarbageTypes::find($createdGarbageTypes['id']), 'GarbageTypes with given id must be in DB');
        $this->assertModelData($garbageTypes, $createdGarbageTypes);
    }

    /**
     * @test read
     */
    public function test_read_garbage_types()
    {
        $garbageTypes = factory(GarbageTypes::class)->create();

        $dbGarbageTypes = $this->garbageTypesRepo->find($garbageTypes->id);

        $dbGarbageTypes = $dbGarbageTypes->toArray();
        $this->assertModelData($garbageTypes->toArray(), $dbGarbageTypes);
    }

    /**
     * @test update
     */
    public function test_update_garbage_types()
    {
        $garbageTypes = factory(GarbageTypes::class)->create();
        $fakeGarbageTypes = factory(GarbageTypes::class)->make()->toArray();

        $updatedGarbageTypes = $this->garbageTypesRepo->update($fakeGarbageTypes, $garbageTypes->id);

        $this->assertModelData($fakeGarbageTypes, $updatedGarbageTypes->toArray());
        $dbGarbageTypes = $this->garbageTypesRepo->find($garbageTypes->id);
        $this->assertModelData($fakeGarbageTypes, $dbGarbageTypes->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_garbage_types()
    {
        $garbageTypes = factory(GarbageTypes::class)->create();

        $resp = $this->garbageTypesRepo->delete($garbageTypes->id);

        $this->assertTrue($resp);
        $this->assertNull(GarbageTypes::find($garbageTypes->id), 'GarbageTypes should not exist in DB');
    }
}
