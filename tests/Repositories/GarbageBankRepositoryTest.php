<?php namespace Tests\Repositories;

use App\Models\GarbageBank;
use App\Repositories\GarbageBankRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class GarbageBankRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var GarbageBankRepository
     */
    protected $garbageBankRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->garbageBankRepo = \App::make(GarbageBankRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_garbage_bank()
    {
        $garbageBank = factory(GarbageBank::class)->make()->toArray();

        $createdGarbageBank = $this->garbageBankRepo->create($garbageBank);

        $createdGarbageBank = $createdGarbageBank->toArray();
        $this->assertArrayHasKey('id', $createdGarbageBank);
        $this->assertNotNull($createdGarbageBank['id'], 'Created GarbageBank must have id specified');
        $this->assertNotNull(GarbageBank::find($createdGarbageBank['id']), 'GarbageBank with given id must be in DB');
        $this->assertModelData($garbageBank, $createdGarbageBank);
    }

    /**
     * @test read
     */
    public function test_read_garbage_bank()
    {
        $garbageBank = factory(GarbageBank::class)->create();

        $dbGarbageBank = $this->garbageBankRepo->find($garbageBank->id);

        $dbGarbageBank = $dbGarbageBank->toArray();
        $this->assertModelData($garbageBank->toArray(), $dbGarbageBank);
    }

    /**
     * @test update
     */
    public function test_update_garbage_bank()
    {
        $garbageBank = factory(GarbageBank::class)->create();
        $fakeGarbageBank = factory(GarbageBank::class)->make()->toArray();

        $updatedGarbageBank = $this->garbageBankRepo->update($fakeGarbageBank, $garbageBank->id);

        $this->assertModelData($fakeGarbageBank, $updatedGarbageBank->toArray());
        $dbGarbageBank = $this->garbageBankRepo->find($garbageBank->id);
        $this->assertModelData($fakeGarbageBank, $dbGarbageBank->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_garbage_bank()
    {
        $garbageBank = factory(GarbageBank::class)->create();

        $resp = $this->garbageBankRepo->delete($garbageBank->id);

        $this->assertTrue($resp);
        $this->assertNull(GarbageBank::find($garbageBank->id), 'GarbageBank should not exist in DB');
    }
}
