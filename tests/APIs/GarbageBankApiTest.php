<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\GarbageBank;

class GarbageBankApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_garbage_bank()
    {
        $garbageBank = factory(GarbageBank::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/garbage_banks', $garbageBank
        );

        $this->assertApiResponse($garbageBank);
    }

    /**
     * @test
     */
    public function test_read_garbage_bank()
    {
        $garbageBank = factory(GarbageBank::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/garbage_banks/'.$garbageBank->id
        );

        $this->assertApiResponse($garbageBank->toArray());
    }

    /**
     * @test
     */
    public function test_update_garbage_bank()
    {
        $garbageBank = factory(GarbageBank::class)->create();
        $editedGarbageBank = factory(GarbageBank::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/garbage_banks/'.$garbageBank->id,
            $editedGarbageBank
        );

        $this->assertApiResponse($editedGarbageBank);
    }

    /**
     * @test
     */
    public function test_delete_garbage_bank()
    {
        $garbageBank = factory(GarbageBank::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/garbage_banks/'.$garbageBank->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/garbage_banks/'.$garbageBank->id
        );

        $this->response->assertStatus(404);
    }
}
