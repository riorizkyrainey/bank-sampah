<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\GarbageTypes;

class GarbageTypesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_garbage_types()
    {
        $garbageTypes = factory(GarbageTypes::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/garbage_types', $garbageTypes
        );

        $this->assertApiResponse($garbageTypes);
    }

    /**
     * @test
     */
    public function test_read_garbage_types()
    {
        $garbageTypes = factory(GarbageTypes::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/garbage_types/'.$garbageTypes->id
        );

        $this->assertApiResponse($garbageTypes->toArray());
    }

    /**
     * @test
     */
    public function test_update_garbage_types()
    {
        $garbageTypes = factory(GarbageTypes::class)->create();
        $editedGarbageTypes = factory(GarbageTypes::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/garbage_types/'.$garbageTypes->id,
            $editedGarbageTypes
        );

        $this->assertApiResponse($editedGarbageTypes);
    }

    /**
     * @test
     */
    public function test_delete_garbage_types()
    {
        $garbageTypes = factory(GarbageTypes::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/garbage_types/'.$garbageTypes->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/garbage_types/'.$garbageTypes->id
        );

        $this->response->assertStatus(404);
    }
}
