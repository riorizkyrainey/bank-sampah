<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\GarbagePriceHistory;

class GarbagePriceHistoryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_garbage_price_history()
    {
        $garbagePriceHistory = factory(GarbagePriceHistory::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/garbage_price_histories', $garbagePriceHistory
        );

        $this->assertApiResponse($garbagePriceHistory);
    }

    /**
     * @test
     */
    public function test_read_garbage_price_history()
    {
        $garbagePriceHistory = factory(GarbagePriceHistory::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/garbage_price_histories/'.$garbagePriceHistory->id
        );

        $this->assertApiResponse($garbagePriceHistory->toArray());
    }

    /**
     * @test
     */
    public function test_update_garbage_price_history()
    {
        $garbagePriceHistory = factory(GarbagePriceHistory::class)->create();
        $editedGarbagePriceHistory = factory(GarbagePriceHistory::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/garbage_price_histories/'.$garbagePriceHistory->id,
            $editedGarbagePriceHistory
        );

        $this->assertApiResponse($editedGarbagePriceHistory);
    }

    /**
     * @test
     */
    public function test_delete_garbage_price_history()
    {
        $garbagePriceHistory = factory(GarbagePriceHistory::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/garbage_price_histories/'.$garbagePriceHistory->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/garbage_price_histories/'.$garbagePriceHistory->id
        );

        $this->response->assertStatus(404);
    }
}
