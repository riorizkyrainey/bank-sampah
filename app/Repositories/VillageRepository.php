<?php

namespace App\Repositories;

use App\Models\Village;
use App\Repositories\BaseRepository;

/**
 * Class VillageRepository
 * @package App\Repositories
 * @version October 26, 2019, 7:32 am UTC
*/

class VillageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'district_id',
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Village::class;
    }
}
