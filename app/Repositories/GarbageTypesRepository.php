<?php

namespace App\Repositories;

use App\Models\GarbageTypes;
use App\Repositories\BaseRepository;

/**
 * Class GarbageTypesRepository
 * @package App\Repositories
 * @version October 25, 2019, 8:01 am UTC
*/

class GarbageTypesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GarbageTypes::class;
    }
}
