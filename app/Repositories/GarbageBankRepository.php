<?php

namespace App\Repositories;

use App\Models\GarbageBank;
use App\Repositories\BaseRepository;

/**
 * Class GarbageBankRepository
 * @package App\Repositories
 * @version October 25, 2019, 8:00 am UTC
*/

class GarbageBankRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'village_id',
        'name',
        'address',
        'active',
        'photo',
        'customer_count',
        'latitude',
        'longitude',
        'pin',
        'information',
        'contact_person'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GarbageBank::class;
    }
}
