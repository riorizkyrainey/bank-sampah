<?php

namespace App\Repositories;

use App\Models\Zones;
use App\Repositories\BaseRepository;

/**
 * Class ZonesRepository
 * @package App\Repositories
 * @version October 25, 2019, 8:21 am UTC
*/

class ZonesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Zones::class;
    }
}
