<?php

namespace App\Repositories;

use App\Models\GarbagePriceHistory;
use App\Repositories\BaseRepository;

/**
 * Class GarbagePriceHistoryRepository
 * @package App\Repositories
 * @version October 25, 2019, 8:01 am UTC
*/

class GarbagePriceHistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'garbage_type_id',
        'garbage_bank_id',
        'date',
        'price'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GarbagePriceHistory::class;
    }
}
