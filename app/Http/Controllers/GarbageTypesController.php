<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGarbageTypesRequest;
use App\Http\Requests\UpdateGarbageTypesRequest;
use App\Repositories\GarbageTypesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class GarbageTypesController extends AppBaseController
{
    /** @var  GarbageTypesRepository */
    private $garbageTypesRepository;

    public function __construct(GarbageTypesRepository $garbageTypesRepo)
    {
        $this->garbageTypesRepository = $garbageTypesRepo;
    }

    /**
     * Display a listing of the GarbageTypes.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $garbageTypes = $this->garbageTypesRepository->all();

        return view('garbage_types.index')
            ->with('garbageTypes', $garbageTypes);
    }

    /**
     * Show the form for creating a new GarbageTypes.
     *
     * @return Response
     */
    public function create()
    {
        return view('garbage_types.create');
    }

    /**
     * Store a newly created GarbageTypes in storage.
     *
     * @param CreateGarbageTypesRequest $request
     *
     * @return Response
     */
    public function store(CreateGarbageTypesRequest $request)
    {
        $input = $request->all();

        $garbageTypes = $this->garbageTypesRepository->create($input);

        Flash::success('Garbage Types saved successfully.');

        return redirect(route('garbageTypes.index'));
    }

    /**
     * Display the specified GarbageTypes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $garbageTypes = $this->garbageTypesRepository->find($id);

        if (empty($garbageTypes)) {
            Flash::error('Garbage Types not found');

            return redirect(route('garbageTypes.index'));
        }

        return view('garbage_types.show')->with('garbageTypes', $garbageTypes);
    }

    /**
     * Show the form for editing the specified GarbageTypes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $garbageTypes = $this->garbageTypesRepository->find($id);

        if (empty($garbageTypes)) {
            Flash::error('Garbage Types not found');

            return redirect(route('garbageTypes.index'));
        }

        return view('garbage_types.edit')->with('garbageTypes', $garbageTypes);
    }

    /**
     * Update the specified GarbageTypes in storage.
     *
     * @param int $id
     * @param UpdateGarbageTypesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGarbageTypesRequest $request)
    {
        $garbageTypes = $this->garbageTypesRepository->find($id);

        if (empty($garbageTypes)) {
            Flash::error('Garbage Types not found');

            return redirect(route('garbageTypes.index'));
        }

        $garbageTypes = $this->garbageTypesRepository->update($request->all(), $id);

        Flash::success('Garbage Types updated successfully.');

        return redirect(route('garbageTypes.index'));
    }

    /**
     * Remove the specified GarbageTypes from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $garbageTypes = $this->garbageTypesRepository->find($id);

        if (empty($garbageTypes)) {
            Flash::error('Garbage Types not found');

            return redirect(route('garbageTypes.index'));
        }

        $this->garbageTypesRepository->delete($id);

        Flash::success('Garbage Types deleted successfully.');

        return redirect(route('garbageTypes.index'));
    }
}
