<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\Village;
use App\Repositories\VillageRepository;
use Illuminate\Http\Request;
use Response;

/**
 * Class VillageController
 * @package App\Http\Controllers\API
 */

class VillageAPIController extends AppBaseController
{
    /** @var  VillageRepository */
    private $villageRepository;

    public function __construct(VillageRepository $villageRepo)
    {
        $this->villageRepository = $villageRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/villages",
     *      summary="Get a listing of the Villages.",
     *      tags={"Village"},
     *      description="Get all Villages",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Village")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $villages = $this->villageRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($villages->toArray(), 'Villages retrieved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/villages/{id}",
     *      summary="Display the specified Village",
     *      tags={"Village"},
     *      description="Get Village",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Village",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Village"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Village $village */
        $village = $this->villageRepository->find($id);

        if (empty($village)) {
            return $this->sendError('Village not found');
        }

        return $this->sendResponse($village->toArray(), 'Village retrieved successfully');
    }

}
