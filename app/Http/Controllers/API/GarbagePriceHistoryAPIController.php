<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\GarbagePriceHistory;
use App\Repositories\GarbagePriceHistoryRepository;
use Illuminate\Http\Request;
use Response;

/**
 * Class GarbagePriceHistoryController
 * @package App\Http\Controllers\API
 */

class GarbagePriceHistoryAPIController extends AppBaseController
{
    /** @var  GarbagePriceHistoryRepository */
    private $garbagePriceHistoryRepository;

    public function __construct(GarbagePriceHistoryRepository $garbagePriceHistoryRepo)
    {
        $this->garbagePriceHistoryRepository = $garbagePriceHistoryRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/garbage_price_histories",
     *      summary="Get a listing of the GarbagePriceHistories.",
     *      tags={"GarbagePriceHistory"},
     *      description="Get all GarbagePriceHistories",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/GarbagePriceHistory")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $garbagePriceHistories = $this->garbagePriceHistoryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($garbagePriceHistories->toArray(), 'Garbage Price Histories retrieved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/garbage_price_histories/{id}",
     *      summary="Display the specified GarbagePriceHistory",
     *      tags={"GarbagePriceHistory"},
     *      description="Get GarbagePriceHistory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of GarbagePriceHistory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="garabage_bank_id",
     *          description="id of GarbageBank",
     *          type="integer",
     *          required=true,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/GarbagePriceHistory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show(Request $request, $id)
    {
        $request->validate([
            'garabage_bank_id' => 'required|integer',
        ]);

        /** @var GarbagePriceHistory $garbagePriceHistory */
        $garbagePriceHistory = $this->garbagePriceHistoryRepository->all([
            'id' => $id,
            'garabage_bank_id' => $request->input('garabage_bank_id'),
        ]);

        if (empty($garbagePriceHistory)) {
            return $this->sendError('Garbage Price History not found');
        }

        return $this->sendResponse($garbagePriceHistory->toArray(), 'Garbage Price History retrieved successfully');
    }

}
