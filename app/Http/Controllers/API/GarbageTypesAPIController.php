<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\GarbageTypes;
use App\Repositories\GarbageTypesRepository;
use Illuminate\Http\Request;
use Response;

/**
 * Class GarbageTypesController
 * @package App\Http\Controllers\API
 */

class GarbageTypesAPIController extends AppBaseController
{
    /** @var  GarbageTypesRepository */
    private $garbageTypesRepository;

    public function __construct(GarbageTypesRepository $garbageTypesRepo)
    {
        $this->garbageTypesRepository = $garbageTypesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/garbage_type",
     *      summary="Get a listing of the GarbageTypes.",
     *      tags={"GarbageTypes"},
     *      description="Get all GarbageTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/GarbageTypes")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $garbageTypes = $this->garbageTypesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($garbageTypes->toArray(), 'Garbage Types retrieved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/garbage_type/{id}",
     *      summary="Display the specified GarbageTypes",
     *      tags={"GarbageTypes"},
     *      description="Get GarbageTypes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of GarbageTypes",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/GarbageTypes"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var GarbageTypes $garbageTypes */
        $garbageTypes = $this->garbageTypesRepository->find($id);

        if (empty($garbageTypes)) {
            return $this->sendError('Garbage Types not found');
        }

        return $this->sendResponse($garbageTypes->toArray(), 'Garbage Types retrieved successfully');
    }

}
