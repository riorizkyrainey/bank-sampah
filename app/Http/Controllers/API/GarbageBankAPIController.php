<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\GarbageBank;
use App\Repositories\GarbageBankRepository;
use Illuminate\Http\Request;
use Response;

/**
 * Class GarbageBankController
 * @package App\Http\Controllers\API
 */

class GarbageBankAPIController extends AppBaseController
{
    /** @var  GarbageBankRepository */
    private $garbageBankRepository;

    public function __construct(GarbageBankRepository $garbageBankRepo)
    {
        $this->garbageBankRepository = $garbageBankRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/garbage_banks",
     *      summary="Get a listing of the GarbageBanks.",
     *      tags={"GarbageBank"},
     *      description="Get all GarbageBanks",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/GarbageBank")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $garbageBanks = $this->garbageBankRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($garbageBanks->toArray(), 'Garbage Banks retrieved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/garbage_banks/{id}",
     *      summary="Display the specified GarbageBank",
     *      tags={"GarbageBank"},
     *      description="Get GarbageBank",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of GarbageBank",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/GarbageBank"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var GarbageBank $garbageBank */
        $garbageBank = $this->garbageBankRepository->find($id);

        if (empty($garbageBank)) {
            return $this->sendError('Garbage Bank not found');
        }

        return $this->sendResponse($garbageBank->toArray(), 'Garbage Bank retrieved successfully');
    }

}
