<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\Zones;
use App\Repositories\ZonesRepository;
use Illuminate\Http\Request;
use Response;

/**
 * Class ZonesController
 * @package App\Http\Controllers\API
 */

class ZonesAPIController extends AppBaseController
{
    /** @var  ZonesRepository */
    private $zonesRepository;

    public function __construct(ZonesRepository $zonesRepo)
    {
        $this->zonesRepository = $zonesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/zones",
     *      summary="Get a listing of the Zones.",
     *      tags={"Zones"},
     *      description="Get all Zones",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Zones")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $zones = $this->zonesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($zones->toArray(), 'Zones retrieved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/zones/{id}",
     *      summary="Display the specified Zones",
     *      tags={"Zones"},
     *      description="Get Zones",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Zones",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Zones"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Zones $zones */
        $zones = $this->zonesRepository->find($id);

        if (empty($zones)) {
            return $this->sendError('Zones not found');
        }

        return $this->sendResponse($zones->toArray(), 'Zones retrieved successfully');
    }

}
