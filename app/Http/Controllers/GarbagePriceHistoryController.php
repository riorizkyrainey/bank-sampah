<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGarbagePriceHistoryRequest;
use App\Http\Requests\UpdateGarbagePriceHistoryRequest;
use App\Repositories\GarbagePriceHistoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class GarbagePriceHistoryController extends AppBaseController
{
    /** @var  GarbagePriceHistoryRepository */
    private $garbagePriceHistoryRepository;

    public function __construct(GarbagePriceHistoryRepository $garbagePriceHistoryRepo)
    {
        $this->garbagePriceHistoryRepository = $garbagePriceHistoryRepo;
    }

    /**
     * Display a listing of the GarbagePriceHistory.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $garbagePriceHistories = $this->garbagePriceHistoryRepository->all();

        return view('garbage_price_histories.index')
            ->with('garbagePriceHistories', $garbagePriceHistories);
    }

    /**
     * Show the form for creating a new GarbagePriceHistory.
     *
     * @return Response
     */
    public function create()
    {
        return view('garbage_price_histories.create');
    }

    /**
     * Store a newly created GarbagePriceHistory in storage.
     *
     * @param CreateGarbagePriceHistoryRequest $request
     *
     * @return Response
     */
    public function store(CreateGarbagePriceHistoryRequest $request)
    {
        $input = $request->all();

        $garbagePriceHistory = $this->garbagePriceHistoryRepository->create($input);

        Flash::success('Garbage Price History saved successfully.');

        return redirect(route('garbagePriceHistories.index'));
    }

    /**
     * Display the specified GarbagePriceHistory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $garbagePriceHistory = $this->garbagePriceHistoryRepository->find($id);

        if (empty($garbagePriceHistory)) {
            Flash::error('Garbage Price History not found');

            return redirect(route('garbagePriceHistories.index'));
        }

        return view('garbage_price_histories.show')->with('garbagePriceHistory', $garbagePriceHistory);
    }

    /**
     * Show the form for editing the specified GarbagePriceHistory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $garbagePriceHistory = $this->garbagePriceHistoryRepository->find($id);

        if (empty($garbagePriceHistory)) {
            Flash::error('Garbage Price History not found');

            return redirect(route('garbagePriceHistories.index'));
        }

        return view('garbage_price_histories.edit')->with('garbagePriceHistory', $garbagePriceHistory);
    }

    /**
     * Update the specified GarbagePriceHistory in storage.
     *
     * @param int $id
     * @param UpdateGarbagePriceHistoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGarbagePriceHistoryRequest $request)
    {
        $garbagePriceHistory = $this->garbagePriceHistoryRepository->find($id);

        if (empty($garbagePriceHistory)) {
            Flash::error('Garbage Price History not found');

            return redirect(route('garbagePriceHistories.index'));
        }

        $garbagePriceHistory = $this->garbagePriceHistoryRepository->update($request->all(), $id);

        Flash::success('Garbage Price History updated successfully.');

        return redirect(route('garbagePriceHistories.index'));
    }

    /**
     * Remove the specified GarbagePriceHistory from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $garbagePriceHistory = $this->garbagePriceHistoryRepository->find($id);

        if (empty($garbagePriceHistory)) {
            Flash::error('Garbage Price History not found');

            return redirect(route('garbagePriceHistories.index'));
        }

        $this->garbagePriceHistoryRepository->delete($id);

        Flash::success('Garbage Price History deleted successfully.');

        return redirect(route('garbagePriceHistories.index'));
    }
}
