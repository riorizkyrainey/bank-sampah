<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGarbageBankRequest;
use App\Http\Requests\UpdateGarbageBankRequest;
use App\Repositories\GarbageBankRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class GarbageBankController extends AppBaseController
{
    /** @var  GarbageBankRepository */
    private $garbageBankRepository;

    public function __construct(GarbageBankRepository $garbageBankRepo)
    {
        $this->garbageBankRepository = $garbageBankRepo;
    }

    /**
     * Display a listing of the GarbageBank.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $garbageBanks = $this->garbageBankRepository->all();

        return view('garbage_banks.index')
            ->with('garbageBanks', $garbageBanks);
    }

    /**
     * Show the form for creating a new GarbageBank.
     *
     * @return Response
     */
    public function create()
    {
        return view('garbage_banks.create');
    }

    /**
     * Store a newly created GarbageBank in storage.
     *
     * @param CreateGarbageBankRequest $request
     *
     * @return Response
     */
    public function store(CreateGarbageBankRequest $request)
    {
        $input = $request->all();

        $garbageBank = $this->garbageBankRepository->create($input);

        Flash::success('Garbage Bank saved successfully.');

        return redirect(route('garbageBanks.index'));
    }

    /**
     * Display the specified GarbageBank.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $garbageBank = $this->garbageBankRepository->find($id);

        if (empty($garbageBank)) {
            Flash::error('Garbage Bank not found');

            return redirect(route('garbageBanks.index'));
        }

        return view('garbage_banks.show')->with('garbageBank', $garbageBank);
    }

    /**
     * Show the form for editing the specified GarbageBank.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $garbageBank = $this->garbageBankRepository->find($id);

        if (empty($garbageBank)) {
            Flash::error('Garbage Bank not found');

            return redirect(route('garbageBanks.index'));
        }

        return view('garbage_banks.edit')->with('garbageBank', $garbageBank);
    }

    /**
     * Update the specified GarbageBank in storage.
     *
     * @param int $id
     * @param UpdateGarbageBankRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGarbageBankRequest $request)
    {
        $garbageBank = $this->garbageBankRepository->find($id);

        if (empty($garbageBank)) {
            Flash::error('Garbage Bank not found');

            return redirect(route('garbageBanks.index'));
        }

        $garbageBank = $this->garbageBankRepository->update($request->all(), $id);

        Flash::success('Garbage Bank updated successfully.');

        return redirect(route('garbageBanks.index'));
    }

    /**
     * Remove the specified GarbageBank from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $garbageBank = $this->garbageBankRepository->find($id);

        if (empty($garbageBank)) {
            Flash::error('Garbage Bank not found');

            return redirect(route('garbageBanks.index'));
        }

        $this->garbageBankRepository->delete($id);

        Flash::success('Garbage Bank deleted successfully.');

        return redirect(route('garbageBanks.index'));
    }
}
