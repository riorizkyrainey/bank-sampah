<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {

    Route::resource('districts', 'DistrictAPIController')->except([
        'create', 'store', 'update', 'destroy',
    ]);

    Route::resource('garbage_banks', 'GarbageBankAPIController')->except([
        'create', 'store', 'update', 'destroy',
    ]);

    Route::resource('garbage_price_histories', 'GarbagePriceHistoryAPIController')->except([
        'create', 'store', 'update', 'destroy',
    ]);

    Route::resource('garbage_types', 'GarbageTypesAPIController')->except([
        'create', 'store', 'update', 'destroy',
    ]);

    Route::resource('villages', 'VillageAPIController')->except([
        'create', 'store', 'update', 'destroy',
    ]);

    Route::resource('zones', 'ZonesAPIController')->except([
        'create', 'store', 'update', 'destroy',
    ]);
});
