<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('districts', 'DistrictController');

Route::resource('garbageBanks', 'GarbageBankController');

Route::resource('garbagePriceHistories', 'GarbagePriceHistoryController');

Route::resource('garbageTypes', 'GarbageTypesController');

Route::resource('villages', 'VillageController');

Route::resource('zones', 'ZonesController');


Route::resource('villages', 'VillageController');