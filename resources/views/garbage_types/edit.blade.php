@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Garbage Types
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($garbageTypes, ['route' => ['garbageTypes.update', $garbageTypes->id], 'method' => 'patch']) !!}

                        @include('garbage_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection