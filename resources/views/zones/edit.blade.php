@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Zones
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($zones, ['route' => ['zones.update', $zones->id], 'method' => 'patch']) !!}

                        @include('zones.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection