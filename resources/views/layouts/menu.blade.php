<li class="{{ Request::is('districts*') ? 'active' : '' }}">
    <a href="{!! route('districts.index') !!}"><i class="fa fa-edit"></i><span>Districts</span></a>
</li>

<li class="{{ Request::is('garbageBanks*') ? 'active' : '' }}">
    <a href="{!! route('garbageBanks.index') !!}"><i class="fa fa-edit"></i><span>Garbage Banks</span></a>
</li>

<li class="{{ Request::is('garbagePriceHistories*') ? 'active' : '' }}">
    <a href="{!! route('garbagePriceHistories.index') !!}"><i class="fa fa-edit"></i><span>Garbage Price Histories</span></a>
</li>

<li class="{{ Request::is('garbageTypes*') ? 'active' : '' }}">
    <a href="{!! route('garbageTypes.index') !!}"><i class="fa fa-edit"></i><span>Garbage Types</span></a>
</li>

<li class="{{ Request::is('villages*') ? 'active' : '' }}">
    <a href="{!! route('villages.index') !!}"><i class="fa fa-edit"></i><span>Villages</span></a>
</li>

<li class="{{ Request::is('zones*') ? 'active' : '' }}">
    <a href="{!! route('zones.index') !!}"><i class="fa fa-edit"></i><span>Zones</span></a>
</li>

<li class="{{ Request::is('villages*') ? 'active' : '' }}">
    <a href="{!! route('villages.index') !!}"><i class="fa fa-edit"></i><span>Villages</span></a>
</li>

