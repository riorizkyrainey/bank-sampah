<div class="table-responsive">
    <table class="table" id="villages-table">
        <thead>
            <tr>
                <th>District Id</th>
        <th>Name</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($villages as $village)
            <tr>
                <td>{!! $village->district_id !!}</td>
            <td>{!! $village->name !!}</td>
                <td>
                    {!! Form::open(['route' => ['villages.destroy', $village->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('villages.show', [$village->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('villages.edit', [$village->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
