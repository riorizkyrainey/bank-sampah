@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Village
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($village, ['route' => ['villages.update', $village->id], 'method' => 'patch']) !!}

                        @include('villages.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection