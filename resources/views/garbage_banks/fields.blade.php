<!-- Village Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('village_id', 'Village Id:') !!}
    {!! Form::number('village_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('active', 'Active:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('active', 0) !!}
        {!! Form::checkbox('active', '1', null) !!}
    </label>
</div>


<!-- Photo Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::textarea('photo', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_count', 'Customer Count:') !!}
    {!! Form::number('customer_count', null, ['class' => 'form-control']) !!}
</div>

<!-- Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('latitude', 'Latitude:') !!}
    {!! Form::number('latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('longitude', 'Longitude:') !!}
    {!! Form::number('longitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Pin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pin', 'Pin:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('pin', 0) !!}
        {!! Form::checkbox('pin', '1', null) !!}
    </label>
</div>


<!-- Information Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('information', 'Information:') !!}
    {!! Form::textarea('information', null, ['class' => 'form-control']) !!}
</div>

<!-- Contact Person Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contact_person', 'Contact Person:') !!}
    {!! Form::text('contact_person', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('garbageBanks.index') !!}" class="btn btn-default">Cancel</a>
</div>
