<!-- Village Id Field -->
<div class="form-group">
    {!! Form::label('village_id', 'Village Id:') !!}
    <p>{!! $garbageBank->village_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $garbageBank->name !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $garbageBank->address !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $garbageBank->active !!}</p>
</div>

<!-- Photo Field -->
<div class="form-group">
    {!! Form::label('photo', 'Photo:') !!}
    <p>{!! $garbageBank->photo !!}</p>
</div>

<!-- Customer Count Field -->
<div class="form-group">
    {!! Form::label('customer_count', 'Customer Count:') !!}
    <p>{!! $garbageBank->customer_count !!}</p>
</div>

<!-- Latitude Field -->
<div class="form-group">
    {!! Form::label('latitude', 'Latitude:') !!}
    <p>{!! $garbageBank->latitude !!}</p>
</div>

<!-- Longitude Field -->
<div class="form-group">
    {!! Form::label('longitude', 'Longitude:') !!}
    <p>{!! $garbageBank->longitude !!}</p>
</div>

<!-- Pin Field -->
<div class="form-group">
    {!! Form::label('pin', 'Pin:') !!}
    <p>{!! $garbageBank->pin !!}</p>
</div>

<!-- Information Field -->
<div class="form-group">
    {!! Form::label('information', 'Information:') !!}
    <p>{!! $garbageBank->information !!}</p>
</div>

<!-- Contact Person Field -->
<div class="form-group">
    {!! Form::label('contact_person', 'Contact Person:') !!}
    <p>{!! $garbageBank->contact_person !!}</p>
</div>

