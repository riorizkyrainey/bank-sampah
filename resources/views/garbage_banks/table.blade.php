<div class="table-responsive">
    <table class="table" id="garbageBanks-table">
        <thead>
            <tr>
                <th>Village Id</th>
        <th>Name</th>
        <th>Address</th>
        <th>Active</th>
        <th>Photo</th>
        <th>Customer Count</th>
        <th>Latitude</th>
        <th>Longitude</th>
        <th>Pin</th>
        <th>Information</th>
        <th>Contact Person</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($garbageBanks as $garbageBank)
            <tr>
                <td>{!! $garbageBank->village_id !!}</td>
            <td>{!! $garbageBank->name !!}</td>
            <td>{!! $garbageBank->address !!}</td>
            <td>{!! $garbageBank->active !!}</td>
            <td>{!! $garbageBank->photo !!}</td>
            <td>{!! $garbageBank->customer_count !!}</td>
            <td>{!! $garbageBank->latitude !!}</td>
            <td>{!! $garbageBank->longitude !!}</td>
            <td>{!! $garbageBank->pin !!}</td>
            <td>{!! $garbageBank->information !!}</td>
            <td>{!! $garbageBank->contact_person !!}</td>
                <td>
                    {!! Form::open(['route' => ['garbageBanks.destroy', $garbageBank->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('garbageBanks.show', [$garbageBank->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('garbageBanks.edit', [$garbageBank->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
