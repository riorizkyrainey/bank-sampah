<!-- Garbage Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('garbage_type_id', 'Garbage Type Id:') !!}
    {!! Form::number('garbage_type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Garbage Bank Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('garbage_bank_id', 'Garbage Bank Id:') !!}
    {!! Form::number('garbage_bank_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::date('date', null, ['class' => 'form-control','id'=>'date']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('garbagePriceHistories.index') !!}" class="btn btn-default">Cancel</a>
</div>
