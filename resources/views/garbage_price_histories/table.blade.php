<div class="table-responsive">
    <table class="table" id="garbagePriceHistories-table">
        <thead>
            <tr>
                <th>Garbage Type Id</th>
        <th>Garbage Bank Id</th>
        <th>Date</th>
        <th>Price</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($garbagePriceHistories as $garbagePriceHistory)
            <tr>
                <td>{!! $garbagePriceHistory->garbage_type_id !!}</td>
            <td>{!! $garbagePriceHistory->garbage_bank_id !!}</td>
            <td>{!! $garbagePriceHistory->date !!}</td>
            <td>{!! $garbagePriceHistory->price !!}</td>
                <td>
                    {!! Form::open(['route' => ['garbagePriceHistories.destroy', $garbagePriceHistory->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('garbagePriceHistories.show', [$garbagePriceHistory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('garbagePriceHistories.edit', [$garbagePriceHistory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
