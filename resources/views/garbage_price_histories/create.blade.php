@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Garbage Price History
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'garbagePriceHistories.store']) !!}

                        @include('garbage_price_histories.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
