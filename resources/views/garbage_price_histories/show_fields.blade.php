<!-- Garbage Type Id Field -->
<div class="form-group">
    {!! Form::label('garbage_type_id', 'Garbage Type Id:') !!}
    <p>{!! $garbagePriceHistory->garbage_type_id !!}</p>
</div>

<!-- Garbage Bank Id Field -->
<div class="form-group">
    {!! Form::label('garbage_bank_id', 'Garbage Bank Id:') !!}
    <p>{!! $garbagePriceHistory->garbage_bank_id !!}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{!! $garbagePriceHistory->date !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $garbagePriceHistory->price !!}</p>
</div>

