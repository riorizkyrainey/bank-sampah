# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.24)
# Database: bank_sampah
# Generation Time: 2019-10-25 11:55:01 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table districts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `districts`;

CREATE TABLE `districts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `zone_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `zone_id` (`zone_id`),
  CONSTRAINT `districts_ibfk_1` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `districts` WRITE;
/*!40000 ALTER TABLE `districts` DISABLE KEYS */;

INSERT INTO `districts` (`id`, `zone_id`, `name`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,'Tambaksari','2019-10-25 09:32:23',NULL,NULL),
	(2,1,'Sukolilo','2019-10-25 09:32:23',NULL,NULL),
	(3,1,'Gubeng','2019-10-25 09:32:23',NULL,NULL),
	(4,1,'Tenggilis Mejoyo','2019-10-25 09:32:23',NULL,NULL),
	(5,1,'Mulyorejo','2019-10-25 09:32:23',NULL,NULL),
	(6,1,'Rungkut','2019-10-25 09:32:23',NULL,NULL),
	(7,1,'Gunung Anyar','2019-10-25 09:32:23',NULL,NULL),
	(8,2,'Pakal','2019-10-25 09:32:23',NULL,NULL),
	(9,2,'Tandes','2019-10-25 09:32:23',NULL,NULL),
	(10,2,'Benowo','2019-10-25 09:32:23',NULL,NULL),
	(11,2,'Sukomanunggal','2019-10-25 09:32:23',NULL,NULL),
	(12,2,'Lakarsantri','2019-10-25 09:32:23',NULL,NULL),
	(13,2,'Sambikerep','2019-10-25 09:32:23',NULL,NULL),
	(14,4,'Jambangan','2019-10-25 09:32:23',NULL,NULL),
	(15,4,'Wonokromo','2019-10-25 09:32:23',NULL,NULL),
	(16,4,'Wonocolo','2019-10-25 09:32:23',NULL,NULL),
	(17,4,'Karang Pilang','2019-10-25 09:32:23',NULL,NULL),
	(18,4,'Gayungan','2019-10-25 09:32:23',NULL,NULL),
	(19,4,'Sawahan','2019-10-25 09:32:23',NULL,NULL),
	(20,4,'Wiyung','2019-10-25 09:32:23',NULL,NULL),
	(21,4,'Dukuh Pakis','2019-10-25 09:32:23',NULL,NULL),
	(22,5,'Tegalsari','2019-10-25 09:32:23',NULL,NULL),
	(23,5,'Bubutan','2019-10-25 09:32:23',NULL,NULL),
	(24,5,'Simokerto','2019-10-25 09:32:23',NULL,NULL),
	(25,5,'Genteng','2019-10-25 09:32:23',NULL,NULL),
	(26,3,'Kenjeran','2019-10-25 09:32:23',NULL,NULL),
	(27,3,'Semampir','2019-10-25 09:32:23',NULL,NULL),
	(28,3,'Bulak','2019-10-25 09:32:23',NULL,NULL),
	(29,3,'Pabean Cantian','2019-10-25 09:32:23',NULL,NULL),
	(30,3,'Krembangan','2019-10-25 09:32:23',NULL,NULL);

/*!40000 ALTER TABLE `districts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table garbage_banks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `garbage_banks`;

CREATE TABLE `garbage_banks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `village_id` int(10) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `active` tinyint(1) DEFAULT '0',
  `photo` tinyint(1) DEFAULT '0',
  `customer_count` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `lat_long` text COLLATE utf8mb4_unicode_ci,
  `pin` tinyint(4) DEFAULT '0',
  `information` text COLLATE utf8mb4_unicode_ci,
  `contact_person` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `garbage_banks` WRITE;
/*!40000 ALTER TABLE `garbage_banks` DISABLE KEYS */;

INSERT INTO `garbage_banks` (`id`, `village_id`, `name`, `address`, `active`, `photo`, `customer_count`, `lat_long`, `pin`, `information`, `contact_person`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,'MITRA KGM','KAPAS GADING MADYA 5/12A',1,0,'± 30 orang','\"7¡14\'15.7\"\"S 112¡46\'28.8\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(2,1,'MANDIRI','LEBAK REJO UTARA 5',1,0,'± 50 orang','\"7¡14\'06.2\"\"S 112¡46\'56.6\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(3,2,'MASIDOSI II','MED.SEM F/25 RT 02',1,0,'± 20 orang','\"7¡18\'18.7\"\"S 112¡47\'45.8\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(4,2,'MASIDOSI VI','MED.SEM M/17 RT 6',1,0,'± 20-25 orang','\"7¡18\'20.5\"\"S 112¡47\'40.1\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(5,2,'MASIDOSI III','MED.SEM H/12 RT 3',1,0,'± 18 orang','\"7¡18\'20.8\"\"S 112¡47\'44.2\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(6,2,'MASIDOSI I','MED.SEM E/34A RT 01',1,0,'± 15 orang','\"7¡18\'23.0\"\"S 112¡47\'45.7\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(7,2,'MASIDOSI IV','MED.SEM J/2 RT 4',1,0,'± 20 orang','\"7¡18\'18.8\"\"S 112¡47\'42.6\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(8,3,'Sri Kandi','RT III RW VIII',0,0,'','\"7¡17\'57.5\"\"S 112¡48\'23.6\"\"E\"',0,'Vakum','\r',NULL,NULL,NULL),
	(9,4,'Menur Nadhif','Jl. Menur II/71',1,0,'± 40 orang','\"7¡17\'27.5\"\"S 112¡45\'53.3\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(10,5,'KEMUNING','Balai RW. 10',1,0,'± 25 orang','\"7¡15\'30.0\"\"S 112¡45\'30.9\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(11,5,'BUGER\'S','Gersikan 6/17',1,0,'± 26-35 orang','\"7¡15\'20.4\"\"S 112¡45\'51.5\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(12,5,'BS GERMAN','Gerbong 19 E',0,0,'','\"7¡15\'50.7\"\"S 112¡45\'09.9\"\"E\"',0,'Tidak Aktif','\r',NULL,NULL,NULL),
	(13,5,'BERTIGA','Jedong',1,0,'± 30 orang','\"7¡15\'20.1\"\"S 112¡45\'38.7\"\"E',0,NULL,NULL,NULL,NULL,NULL),
	(14,6,'Tuwowo Berseri','Jl. Tuwowo 3F',1,0,'± 80 orang','\"7¡14\'37.4\"\"S 112¡46\'06.3\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(15,7,'merpati tak pernah ingkar janji','karang asem I',1,0,'','\"7¡15\'07.7\"\"S 112¡46\'05.6\"\"E\"',1,'Aktif tapi sifatnya umum','\r',NULL,NULL,NULL),
	(16,8,'Untung Bersama','RT XI RW III',1,0,'± 30 orang','\"7¡14\'39.7\"\"S 112¡46\'38.8\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(17,8,'Euphorbia','Jl. Dukuh Setro I Tengah No. 16 RT I RW VIII',1,0,'± 50 orang','\"7¡14\'25.4\"\"S 112¡46\'23.1\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(18,9,'KAWAN','Jl Jojoran III No 30',1,0,'± 35 orang','\"7¡16\'34.4\"\"S 112¡46\'03.1\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(19,10,'Gotong Royong','RT 5 RW 8',1,0,'± 25-30 orang','\"7¡17\'43.5\"\"S 112¡45\'33.3\"\"E',0,NULL,NULL,NULL,NULL,NULL),
	(20,10,'Barata Berjaya','RT 4 RW 5',1,0,'± 10-12 orang','\"7¡17\'56.5\"\"S 112¡45\'29.5\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(21,10,'SGS','RT 2 RW 4',1,0,'± 20 orang','\"7¡18\'06.1\"\"S 112¡45\'19.3\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(22,10,'Mandiri','RT 2 RW 6',1,0,'± 28 orang','\"7¡18\'15.6\"\"S 112¡45\'23.5\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(23,10,'Makmur Jaya','RT 4 RW 4',1,0,'± 45 orang','\"7¡17\'59.1\"\"S 112¡45\'21.6\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(24,11,'Guja Mandiri','Jl Gubeng Jaya gg 6',1,0,'± 30 orang','\"7¡16\'21.3\"\"S 112¡45\'12.7\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(26,12,'Gemah Ripah','RT 6 RW 4',0,0,'','',0,'','\r',NULL,NULL,NULL),
	(32,16,'Sejahtera','Jl. Mulyorejo Selatan Br No. 53 RT 3 RW 12',0,0,'','',0,'','\r',NULL,NULL,NULL),
	(34,16,'Sari Arta','Jl. Manyar Tegal',0,0,'','',0,'','\r',NULL,NULL,NULL),
	(36,16,'Manyar Berkah','Jl. Manyar Tegal',0,0,'','',0,'','\r',NULL,NULL,NULL),
	(37,17,'Cinta','Jl Medayu Utara XIV A',0,0,'','',0,'','\r',NULL,NULL,NULL),
	(38,17,'Lili','Jl Medayu Utara XVII',0,0,'','',0,'','\r',NULL,NULL,NULL),
	(40,18,'Melati Asri','Jl. Rungkut Asri Barat 1/29 RT 7 RW 7',0,0,'','',0,'','\r',NULL,NULL,NULL),
	(42,18,'Anyelir 2','Jl. Rungkut Asri Timur VII/30',0,0,'','',0,'','\r',NULL,NULL,NULL),
	(43,19,'Salevra','Jl. Rungkut lor 7',1,0,'30 orang','-7.3255525, 112.7702071',1,'','\r',NULL,NULL,NULL),
	(44,20,'makmur sejahtera','Jl. Kedung Baruk VI / 20A RT 1 RW 4',1,0,'55 orang','-7.3130123, 112.7751953',1,'','\r',NULL,NULL,NULL),
	(45,21,'Rukun Makmur','Jl. Rungkut Permai RT I RW VI',1,0,'97 orang','-7.3345007, 112.7706905',1,'','\r',NULL,NULL,NULL),
	(46,21,'Karang Taruna','Jl. Rungkut Teengah III RW 5',0,0,'','',0,'','\r',NULL,NULL,NULL),
	(47,22,'Bank Sampah Sumber Dana','Jl Wisma Indah k3 No 8',1,0,'75 orang','-7.3361258, 112.8130852',1,'','\r',NULL,NULL,NULL),
	(48,22,'Bank Sampah Bintang Mangrove','Jl Gununganyar Tambak III',1,0,'218 orang','-7.3456265, 112.8073255',1,'','\r',NULL,NULL,NULL),
	(49,23,'Sumber Sugih','RT 7 RW 1',1,0,'60 orang','-7.3368444, 112.7652577',1,'','\r',NULL,NULL,NULL),
	(50,24,'Veteran Jaya','RW V',1,1,'30','\"7¡14\'35.9\"\"S 112¡37\'20.4\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(51,24,'Merak','Jl. Pakal Sumberan Baru 4 RT 4 RW 6',1,1,'46','\"7¡14\'32.6\"\"S 112¡37\'07.9\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(52,25,'Mulya Jaya','Jl. Mulyorejo Baru RW 6',0,0,'-','-',0,'-','\r',NULL,NULL,NULL),
	(53,25,'Multi Guna','Pondok Benowo Indah Blok CF-6 RT 3 RW 9',1,1,'62','\"7¡13\'53.1\"\"S 112¡37\'32.3\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(54,25,'Adi Guna','Pondok Benowo Indah RT 4 RW 9',1,1,'60','\"7¡14\'07.2\"\"S 112¡37\'38.5\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(55,25,'Prestasi','Pondok Benowo Indah Blok CP-23 RT 6 RW 9',1,1,'53','\"7¡14\'01.5\"\"S 112¡37\'34.6\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(56,25,'Griya Bersih 6','Griya Benowo Indah RT 6 RW 13',1,1,'35','\"7¡14\'26.2\"\"S 112¡37\'47.6\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(57,25,'Adi Guna','Pondok Benowo Indah RT 9 RW 9',1,1,'30','\"7¡13\'58.8\"\"S 112¡37\'35.5\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(58,25,'Berkah','Pondok Benowo Indah A9/17 RT 1 RW 11',1,1,'56','\"7¡14\'14.5\"\"S 112¡37\'25.7\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(59,25,'Rukun Agawe Santoso','Pondok Benowo Indah RT 4 RW 11',1,1,'60','\"7¡13\'54.9\"\"S 112¡37\'31.0\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(60,25,'Ertiga Berbunga','Pondok Benowo Indah A4/28 RT 3 RW 11',1,1,'41','\"7¡14\'00.2\"\"S 112¡37\'29.9\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(61,25,'Lumbung Jaya','Pondok Benowo Indah RT 5 RW 1',0,0,'-','\"7¡14\'14.0\"\"S 112¡37\'56.7\"\"E\"',1,'-','\r',NULL,NULL,NULL),
	(62,25,'Mulya Abadi','Jl. Mulyorejo Baru RT 5 RW 6',1,1,'80','\"7¡14\'40.9\"\"S 112¡37\'50.9\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(63,25,'Sumber Makmur','Pondok Benowo Indah RT 4 RW 10',1,1,'7','\"7¡14\'12.3\"\"S 112¡37\'57.7\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(64,25,'Gemah Ripah','Pondok Benowo Indah RT 1 RW 12',1,1,'53','\"7¡14\'00.8\"\"S 112¡37\'33.6\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(65,25,'Rejeki','Pondok Benowo Indah RT 2 RW 9',1,1,'32','\"7¡13\'56.1\"\"S 112¡37\'36.4\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(66,25,'Adi Guna','Pondok Benowo Indah RT 8 RW 9',1,1,'30','\"7¡14\'07.8\"\"S 112¡37\'32.8\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(67,25,'Berseri','Pondok Benowo Indah RT 2 RW 11',1,1,'31','\"7¡14\'05.7\"\"S 112¡37\'28.5\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(68,26,'Palem','Jl Balongsari 9 blok 9a',1,1,'15','\"7¡15\'51.7\"\"S 112¡40\'39.8\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(69,26,'Pendowo Limo','Jl Balongsari Tama 9b/6',1,1,'20','\"7¡15\'52.5\"\"S 112¡40\'37.8\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(70,26,'Kartini','Balai RT 6 RW III',1,1,'23','\"7¡15\'54.6\"\"S 112¡40\'41.3\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(71,26,'Cendana','Jl Balongsari Tama Blok 8 E',1,1,'18','\"7¡15\'53.1\"\"S 112¡40\'36.7\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(72,26,'Sukses Bersama','Jl Balongsari Tama Sel',1,1,'21','\"7¡15\'52.8\"\"S 112¡40\'35.8\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(73,26,'Melati','Jl Balongsari Praja III/10',1,1,'33','\"7¡16\'00.2\"\"S 112¡40\'39.4\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(74,27,'Anggrek','Jalan Manukan Indah VII RT 9 RW 3',1,1,'70an nasabah','-7.2665820, 112.6650910',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu langsung dijual ke pengepul tanpa disimpan terlebih dahulu di gudang',NULL,NULL,NULL,NULL),
	(75,27,'PHBS RT 7 RW 3','Jalan Manukan Indah VI RT 7 RW 3',0,0,'87 nasabah','-7.2657480, 112.6647980',0,'\"- Sampah dikumpulkan dan ditimbang setiap 1 minggu sekali lalu langsung dijual ke pengepul tanpa disimpan terlebih dahulu di gudang',NULL,NULL,NULL,NULL),
	(76,27,'Guyub Tertib','Balai RW 8 Jalan Manukan Tirto',1,1,'30an nasabah','-7.2708590, 112.6669110',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu disimpan terlebih dahulu di gudang Balai RW 8',NULL,NULL,NULL,NULL),
	(77,27,'Mukti Jaya','Jalan Manukan Mukti II XI-B no. 14 RT 2 RW 9',1,1,'43 nasabah','-7.2607440, 112.6611830',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu langsung dijual ke pengepul tanpa disimpan terlebih dahulu di gudang',NULL,NULL,NULL,NULL),
	(78,28,'Adi Daya 79','Jalan Manukan Adi I',0,1,'100an nasabah','-7.2626470, 112.6609800',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu disimpan terlebih dahulu di gudang',NULL,NULL,NULL,NULL),
	(79,28,'PHBS RT 6 RW 3','Jalan Manukan Bhakti XII RT 6 RW 3',1,1,'50an nasabah','-7.267019, 112.664060',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu disimpan terlebih dahulu di gudang',NULL,NULL,NULL,NULL),
	(80,28,'Berkah','Jalan Sikatan XII RT 5 RW 1',0,0,'30an nasabah','-7.260528, 112.673686',0,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu langsung dijual tanpa penyimpanan terlebih dahulu',NULL,NULL,NULL,NULL),
	(81,28,'Tunas Muda','Jalan Bibis Tama II Balai RT 1 RW 6',1,1,'40an nasabah','-7.2593670, 112.6770910',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu disimpan terlebih dahulu di gudang sebelum dijual ke pengepul',NULL,NULL,NULL,NULL),
	(82,28,'Ayu Mandiri','Jalan Manukan Madya II Balai RW 3',1,1,'20 nasabah','-7.2612840, 112.6719490',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu disimpan terlebih dahulu di gudang sebelum dijual ke pengepul',NULL,NULL,NULL,NULL),
	(83,28,'Makmur','Jalan Sikatan XVII RT 7 RW 1',1,1,'10an nasabah','-7.2616320, 112.6730800',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu langsung dijual ke pengepul tanpa disimpan terlebih dahulu di gudang',NULL,NULL,NULL,NULL),
	(84,28,'Tunas Mulya','Jalan Bibis Tama V RT 3 RW 6',0,0,'20an nasabah','-7.260584, 112.675499',0,'\"- Kendalanya tidak ada gudang dan partisipasi warga kurang',NULL,NULL,NULL,NULL),
	(85,29,'Panja Jaya','Jalan Gadel Tengah II RT 5 RW 6',0,0,'100an nasabah','-7.2711200, 112.6763700',0,'\"- Kendalanya warga enggan pergi menimbang dan mengumpulkan sampah sehingga para pengurus kerepotan menjemput sampah di rumah-rumah warga',NULL,NULL,NULL,NULL),
	(86,29,'Datar','Jalan Raya Tubanan Baru Blok U RT 2 RW7',1,1,'10an nasabah','-7.2677350, 112.6849430',1,'\"- Sampah dikumpulkan tiap bulan lalu disimpan di balai RW lalu dijual ke pengepul',NULL,NULL,NULL,NULL),
	(87,29,'Maju Makmur','Jalan Gadel Tengah I RT 7 RW 6',1,1,'30 nasabah','-7.2697990, 112.6769540',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu langsung dijual ke pengepul tanpa disimpan terlebih dahulu di gudang',NULL,NULL,NULL,NULL),
	(88,30,'Blueberry','Jalan Gedangasin II RT 3 dan 4 RW 9',1,1,'38 nasabah','-7.2596310, 112.6859030',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu langsung dijual ke pengepul tanpa disimpan terlebih dahulu di gudang',NULL,NULL,NULL,NULL),
	(89,31,'Merah Putih','Jalan Manukan Lor IV A Balai RT 1 RW 1',1,1,'10an nasabah','-7.2588460, 112.6621180',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu langsung dijual ke pengepul tanpa disimpan terlebih dahulu di gudang',NULL,NULL,NULL,NULL),
	(90,31,'Mawar Lestari','Jalan Manukan Lor VI A no. 27-29 RT 5 RW 2',1,1,'61 nasabah','-7.2571730, 112.6625220',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu disimpan terlebih dahulu di gudang untuk dijual ke pengepul',NULL,NULL,NULL,NULL),
	(91,31,'Hijau Mandiri','Jalan Manukan Lor IV E, Balai RT 2 RW 1',1,1,'40an nasabah','-7.2593260, 112.6616010',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu disimpan terlebih dahulu di gudang untuk dijual ke pengepul',NULL,NULL,NULL,NULL),
	(92,31,'Seruni Mekarsari','Jalan Manukan Lor IV G, RT 8 RW 3',1,1,'60an nasabah','-7.2590040, 112.6596400',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu langsung dijual ke pengepul tanpa disimpan terlebih dahulu di gudang',NULL,NULL,NULL,NULL),
	(93,31,'Gotong Royong','Jalan Manukan Lor IV G, RT 4 RW 1',0,0,'30an nasabah','-7.2569180, 112.6602510',0,'Sudah lama tidak aktif karena partisipasi warga yang sangat kurang','Pak RT 4\r',NULL,NULL,NULL),
	(94,31,'Mandiri','Jalan Manukan Lor VII G, RT 4 RW 3',1,1,'40an nasabah','-7.2590030, 112.6611261',1,'\"- Sampah dikumpulkan dan ditimbang setiap 1 bulan sekali lalu langsung dijual ke pengepul tanpa disimpan terlebih dahulu di gudang',NULL,NULL,NULL,NULL),
	(95,31,'Wijaya Kusuma','Jalan Banjarsugihan III no.23 RW 4',0,0,'20an nasabah','-7.2541920, 112.6564830',0,'Sudah 2 tahun tidak aktif karena partisipasi warga yang sangat kurang dan wilayah yang luas yaitu satu RW sehingga pengumpulan sampah sulit dilakukan','Ibu Kholifa\r',NULL,NULL,NULL),
	(96,32,'Romo Berkarya','Jalan Romokalisari dekat Kantor Lurah Romokalisari, RW 1',0,0,'30an nasabah','-7.1981410, 112.6470020',0,'Sudah lama tidak aktif karena partisipasi warga yang sangat kurang dan wilayah yang luas yaitu satu RW sehingga pengumpulan sampah sulit dilakukan','Pak RW\r',NULL,NULL,NULL),
	(97,32,'Romo Asri','Jalan Romokalisari RT 2 RW 2',0,0,'20an nasabah','-7.1955330, 112.6457650',0,'\"- Sudah 2 tahun tidak aktif karena warga enggan mengumpulkan sampah di rumah menyebabkan kotor',NULL,NULL,NULL,NULL),
	(98,33,'MANDIRI','Simorejo XXXV No. 14',1,1,'','\"7¡15\'35.9\"\"S 112¡42\'37.0\"\"E\"',1,'','Bu Yabes - 087808671767\r',NULL,NULL,NULL),
	(99,33,'BERSIH RUKUN','Simorukun 6 No.3 RT 6 RW 4',1,1,'','\"7¡15\'43.6\"\"S 112¡42\'39.2\"\"E\"',1,'','Bu Susi Sjach - 081230143075\r',NULL,NULL,NULL),
	(100,34,'ASOKA MANDIRI','Simorejo Sari B XIV No. 7 RT 10 RW 7',1,1,'','\"7¡15\'33.7\"\"S 112¡42\'20.7\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(101,34,'KAMPOENG SONGO','Simo Hilir I RT 9 RW 3',1,1,'','\"7¡16\'26.3\"\"S 112¡42\'15.6\"\"E\"',1,'','Bu Ika - 082264180675\r',NULL,NULL,NULL),
	(102,34,'WARTUN SEJAHTERA','Jl. Simo Pomahan Baru Barat no 64 RT 6 RW 5',1,1,'','\"7¡15\'37.2\"\"S 112¡42\'16.8\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(103,35,'TANJUNGSARI MANDIRI','Jl. Tanjungsari Gang 3 No. 14 RT 4 RW 2',1,1,'','\"7¡15\'40.6\"\"S 112¡41\'31.6\"\"E\"',1,'','Bu Liliek - 085321459328\r',NULL,NULL,NULL),
	(104,35,'TANJUNGSARI BAHAGIA','Jl. Tanjungsari Gang 3 No. 103 RT 3 RW 2',1,1,'','\"7¡15\'39.7\"\"S 112¡41\'22.9\"\"E\"',1,'','Bu Ani - 0851032327116\r',NULL,NULL,NULL),
	(105,36,'MANDIRI','Lakarsantri RT 01 RW 04',1,1,'','\"7¡17\'54.3\"\"S 112¡38\'10.4\"\"E\"',1,'','Bu Dian - 0895336384564\r',NULL,NULL,NULL),
	(106,36,'MANDIRI','Lakarsantri RT 02 RW 02',0,0,'','',0,'','\r',NULL,NULL,NULL),
	(107,36,'WIJAYA KUSUMA','Lakarsantri RT 01 RW 01',1,1,'','\"7¡18\'12.1\"\"S 112¡38\'08.2\"\"E\"',1,'','Bu Kutipah - 085101691689\r',NULL,NULL,NULL),
	(108,37,'MANDIRI','Jl Raya Sawo RT 01 RW 02',1,1,'','\"7¡15\'58.3\"\"S 112¡38\'46.8\"\"E\"',1,'','Bu Mia - 081216149392\r',NULL,NULL,NULL),
	(109,38,'Karin 4','Jl Karah Indah RT 4 RW I',1,0,'','-7.312265, 112.719761',1,'','\r',NULL,NULL,NULL),
	(110,38,'BISKAR (Bibis Karah)','Karah',0,0,'','',0,'Alamat tidak ditemukan','\r',NULL,NULL,NULL),
	(111,39,'Rukun Jaya','Jl. Jambangan Sawah RT 2 RW III',1,1,'70 orang','-7.321920, 112.712852',1,'','\r',NULL,NULL,NULL),
	(112,39,'Lidah Buaya','Jl. Jambangan VII E RT 3 RW III',1,1,'27 orang','-7.323560, 112.713050',1,'Pengepul Pak Mad di Kebonsari','Bu Yanti\r',NULL,NULL,NULL),
	(113,39,'BSM 46','Jl. Jambangan VII A RT IV RW III',1,1,'','-7.323309, 112.711849',1,'','\r',NULL,NULL,NULL),
	(114,39,'Wolu','Jl. Jambangan Kebon Agung Asri I RT 8 RW III',1,1,'','-7.324266, 112.713578',1,'','\r',NULL,NULL,NULL),
	(115,39,'Diansati RT 3','Jl. Jambangan Sawah RT 3 RW II',1,1,'59 orang','-7.319075, 112.712636',1,'','\r',NULL,NULL,NULL),
	(116,39,'Girly','Jl. Jambangan X RT 5 RW III',1,1,'','-7.322750, 112.710515',1,'','\r',NULL,NULL,NULL),
	(117,39,'Enam','Jl. Jambangan RT 6 RW III',1,1,'','-7.324147, 112.713186',1,'','\r',NULL,NULL,NULL),
	(118,39,'Mandiri','Jl. Jambangan Sawah RT 3 RW I',1,1,'','-7.317581, 112.713285',1,'','\r',NULL,NULL,NULL),
	(119,39,'Siji','Jl. Jambangan Sawah RT 1 RW III',1,1,'','-7.321864, 112.712684',1,'','\r',NULL,NULL,NULL),
	(120,40,'RT VII RW III','Pagesangan asri X/ 29',1,1,'','-7.335754, 112.712985',1,'','\r',NULL,NULL,NULL),
	(121,40,'guyup rukun','pagesangan 3b/23',1,1,'53 orang','-7.335088, 112.708941',1,'pengepul dari sukodono','\r',NULL,NULL,NULL),
	(122,40,'gesang guyup','pagesangan III Buntu',1,1,'100 orang','-7.334693, 112.709249',1,'','\r',NULL,NULL,NULL),
	(123,40,'IJO RESIK','Pagesangan 4/2',1,1,'','-7.336649, 112.710515',1,'','\r',NULL,NULL,NULL),
	(124,40,'JAYA ASRI','PAGESANGAN ASRI VI',1,1,'','-7.337951, 112.711085',1,'','\r',NULL,NULL,NULL),
	(125,40,'hidup sejahtera','pagesangan I',1,1,'43 orang','-7.332151, 112.710461',1,'Pengepul dari Bank Sampah Induk Bina Mandiri','Bu Isma W 085606161632\r',NULL,NULL,NULL),
	(126,40,'Sehati','jl pagesangan I/ Kebonsari 7/10 b',1,1,'75 orang','-7.331252, 112.711408',1,'','\r',NULL,NULL,NULL),
	(127,41,'Kedondong','Jl. Kebonsari Tengah Gg. Manunggal Tirto RT 10 RW 1',1,1,'40 orang','-7.327449, 112.713619',1,'','\r',NULL,NULL,NULL),
	(128,41,'Manggis','Jl. Kebonsari Tengah Gg. Sejati RT 3 RW 1',1,1,'50 orang','-7.324661, 112.713981',1,'','\r',NULL,NULL,NULL),
	(129,41,'Apel','Jl. Kebonsari II RT 5 RW 1',1,1,'','-7.325968, 112.712518',1,'','\r',NULL,NULL,NULL),
	(130,41,'Tumbuh Kembang','Jl. Kebonsari III RT 7 RW 1',1,1,'','-7.326922, 112.711909',1,'','\r',NULL,NULL,NULL),
	(131,41,'Sumber Rejeki','Jl. Kebonsari Baru Selatan RT 8 RW 3',1,1,'60 orang','-7.331362, 112.714837',1,'','\r',NULL,NULL,NULL),
	(132,41,'Semangka','Jl. Kebonsari Gg. I RT 2 RW 1',1,1,'100 orang','-7.324991, 112.712893',1,'','\r',NULL,NULL,NULL),
	(133,41,'Melati Putih','Jl. Kebonsari II RT 4 RW 1',1,1,'50 orang','-7.325689, 112.711213',1,'','\r',NULL,NULL,NULL),
	(134,41,'Delima Ceria','Jl. Kebonsari IIA No. 11 RT 8 RW 1',1,1,'31 orang','-7.326777, 112.713326',1,'','\r',NULL,NULL,NULL),
	(135,41,'Lestari','Jl. Kebonsari Tengah RT 9 RW 1',1,1,'','-7.325284, 112.712970',1,'','\r',NULL,NULL,NULL),
	(136,41,'Mangga','Jl. Kebonsari I RT 1 RW 1',1,1,'','-7.324429, 112.711103',1,'','\r',NULL,NULL,NULL),
	(137,41,'Sarinem','Jl. Kebonsari II RT 6 RW 1',1,1,'','-7.326159, 112.713278',1,'','\r',NULL,NULL,NULL),
	(138,41,'Merapat (Kartar)','Jl. Kebonsari VII - A ( Pos Kamling )',1,1,'62 orang','-7.331368, 112.713404',1,'','\r',NULL,NULL,NULL),
	(139,42,'Cendrawasih','Jl. Gunungsari II RT 5 RW 8',0,0,'','-7.305783, 112.723186',0,'','\r',NULL,NULL,NULL),
	(140,42,'Melati','Jl. Gunungsari II RT 2 RW 8',1,1,'15 orang','-7.306617, 112.724259',1,'Pengepul di Embong Bunder','Bu Siti Aisyah 082244491600\r',NULL,NULL,NULL),
	(141,42,'Mekar Asri','Jl. Gunungsari II RT 6 RW 8',1,0,'44 orang','-7.302873, 112.727189',1,'Pengepul Keris Kencana','Bu Dayat 087856401928 / Bu Kartika 081235734775\r',NULL,NULL,NULL),
	(142,43,'Guyub Sayekti','Jl. Ngagel Mulyo I A',1,1,'50 orang','-7.296148, 112.747621',1,'Pengepul dari Bank Sampah Induk Bina Mandiri','Bu Anik 08155020963\r',NULL,NULL,NULL),
	(143,43,'Mulyo Rejo','Jl. Ngagel Mulyo 6/8',1,1,'50 orang','-7.296141, 112.747681',1,'Pengepul dari Bank Sampah Induk Bina Mandiri','Bu Selima\r',NULL,NULL,NULL),
	(144,44,'JW Project','Jl Jetis Wetan 4',1,1,'50 orang','-7.312570, 112.735940',1,'pengepul dari bina mandiri','\r',NULL,NULL,NULL),
	(145,44,'Karya Mandiri','Jl Margorejo sawah No e',1,1,'','-7.317011, 112.739096',1,'','\r',NULL,NULL,NULL),
	(146,45,'Sejati','G. Keb. Utara , RT 4 RW 9',1,1,'50 orang','-7.325628, 112.700082',1,'','\r',NULL,NULL,NULL),
	(147,45,'Gemilang ( Gemi Lan Gigih )','Kemlaten XII C Musholah',1,1,'','-7.328028, 112.706643',1,'','\r',NULL,NULL,NULL),
	(148,45,'Kebraon RW 4','Balai RW 4',1,1,'','-7.329905, 112.698675',1,'','\r',NULL,NULL,NULL),
	(149,45,'Mulyo Bareng','RT 03, RW V Kebraon , Karang Pilang',1,1,'','-7.331171, 112.705254',1,'','\r',NULL,NULL,NULL),
	(150,45,'Simpan Sampah','Griya Kebraon Praja Barat RC 10 Surabaya',1,1,'','-7.332624, 112.697926',1,'','\r',NULL,NULL,NULL),
	(151,46,'kumbang','gang kawi RT 5 RW 3',1,1,'','-7.342918, 112.691297',1,'','\r',NULL,NULL,NULL),
	(152,46,'melati bersemi','gang Melati 21 RT 7 RW 1',1,1,'','-7.341423, 112.695726',1,'','\r',NULL,NULL,NULL),
	(153,46,'sekarjaya','gang melati 2RT 4 RW 1',1,1,'33 orang','-7.340748, 112.695126',1,'pengepul dari Pak Jasman','\r',NULL,NULL,NULL),
	(154,47,'Lestari','Kedurus Gang III SD No. 40',0,0,'','-7.316874, 112.708160',0,'sudah tidak aktif karena tidak ada pengepul yang mau ambil ke tempat warga','\r',NULL,NULL,NULL),
	(155,47,'Adenium Mandiri','Gayungan RW 1',1,1,'','-7.337620, 112.726935',1,'','\r',NULL,NULL,NULL),
	(156,48,'GKS','Perumahan Gayung Kebonsari jln GKS IX/ 21',1,1,'45 orang','-7.325618, 112.721878',1,'Pengepul dari Senkom Rescue Mitra Polri','Bu Sis Darussalam 082140973773\r',NULL,NULL,NULL),
	(157,49,'Mekar Jaya','Jl. Kedungdoro 9/28',1,1,'22 orang','-7.261954, 112.728023',1,'Pengepul dari Pengepul Bukit Barisan','Bu Kam\r',NULL,NULL,NULL),
	(158,50,'SEJAHTERA','SGBJ H/51 RT 4 RW 15',1,1,'60 orang','-7.272594, 112.714265',1,'Pengepul Pak Ali di Sambikerep','Bu Nur 082230744810\r',NULL,NULL,NULL),
	(159,51,'BAKTI PERTIWI','Taman Pondok Indah Blok TX No.5 RT 5 RW 7',1,1,'','\"7¡19\'10.1\"\"S 112¡41\'49.2\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(160,52,'DUA ENAM','RT 2 RW 6',1,1,'','\"7¡19\'39.6\"\"S 112¡41\'15.3\"\"E\"',1,'','Bu Tinuk - 0881703086222\r',NULL,NULL,NULL),
	(161,53,'SEKTORAL ANGGREK','Jl. Babatan Pilang Gang 3',1,1,'','\"7¡18\'43.4\"\"S 112¡40\'57.8\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(162,53,'MELATI','Jl. Babatan Pilang RT 1 RW 5',0,0,'','',0,'','\r',NULL,NULL,NULL),
	(163,54,'MANDIRI','RT 2 RW 6',1,1,'','\"7¡18\'54.8\"\"S 112¡41\'57.1\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(164,54,'HARAPAN KITA','Gogor Makam RT 3 RW 2',1,1,'','\"7¡18\'38.2\"\"S 112¡42\'32.5\"\"E\"',1,'','Bu Martini - 081216634097\r',NULL,NULL,NULL),
	(165,54,'TUNAS MEKAR 2','RT 1 RW 1',1,1,'','\"7¡18\'28.8\"\"S 112¡42\'44.9\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(166,54,'TUNAS MEKAR 1','RT 2 RW 1',1,1,'','\"7¡18\'28.5\"\"S 112¡42\'42.1\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(167,55,'BHAKTI MULIA','Dukuh Kupang Gang Lebar RT 1 RW 7',1,1,'','\"7¡16\'44.9\"\"S 112¡43\'02.0\"\"E\"',1,'','\r',NULL,NULL,NULL),
	(168,55,'BUNGA BERSERI','Dukuh Pakis 5 No. 47',0,0,'','',0,'','\r',NULL,NULL,NULL),
	(169,56,'Sejahtera','Jl. Kp Malang Utara, RT. 006',0,0,'','-',0,'','\r',NULL,NULL,NULL),
	(170,56,'Mandiri Sejahtera','Jl. Kp Malang Utara, RT. 007 RW. 004',0,0,'','-',0,'','\r',NULL,NULL,NULL),
	(171,57,'Bina Warga','Jl. Dinoyo, RT. 005, RW. 004',1,1,'','-7.2843247, 112.7422756',1,'Setiap minggu, jumlah nasabah sekitar 50\'an orang, buka tanggal 2','\r',NULL,NULL,NULL),
	(172,57,'Mulya Jaya','Jl. Wonorejo IV/102 RT.004 RW. 006',1,1,'','-7,2710220, 112,7305890',1,'Jumlah nasabah 60 orang','\r',NULL,NULL,NULL),
	(173,57,'Rejo Asri 76','Jl. Wonorejo IV/73',1,1,'','-7,2716940, 112,7318340',1,'\"1. Setiap 2 minggu sekali',NULL,NULL,NULL,NULL),
	(174,57,'Mas Pecah','Jl. Wonorejo II/27 RT. 02, RW. 04',1,1,'','-7.2694087, 112.7339862',1,'Sebulan sekali','\r',NULL,NULL,NULL),
	(175,57,'Lancar Jaya','Rusun Wonorejo',0,0,'','',0,'','\r',NULL,NULL,NULL),
	(176,59,'Mekar Jaya','Kedungdoro Gg. 9/ 12C',1,1,'19 orang','-7,2619540, 112,7280230',1,'','\r',NULL,NULL,NULL),
	(177,60,'Sri Ratu','Jl. Asem Jaya RT. 009, RW. 004',1,1,'','-7.251069, 112.718970',1,'Kegiatan dilakukan 1 bulan 2 x','\r',NULL,NULL,NULL),
	(178,60,'Teduh Makmur','Jl. Asem Jajar IX, RT. 009 RW. 003',1,1,'','-7.252479, 112.718295',1,'','\r',NULL,NULL,NULL),
	(179,60,'Dewi Sartika','Jl. Asem Jajar X, RT. 010 RW. 003',1,1,'','-7.252201, 112.718397',1,'\"1. Sampah dipilah dan dikumpulkan, lalu dijual, namun belum ada buku nasabah.',NULL,NULL,NULL,NULL),
	(180,60,'Bersih','Jl. Demak Jaya Gg. X RT 12. RW 10',1,1,'','-7.247930, 112.717669',1,'','\r',NULL,NULL,NULL),
	(181,60,'Pucuk Merah','Jl. Kalibutuh Barat III, RT. 06 RW. 06',1,1,'','-7.256196, 112.721268',1,'','\r',NULL,NULL,NULL),
	(182,60,'Sumber Barokah','Jl. Dupak Jaya Gg. VI RT. 006, RW. 00I',1,1,'30 orang','-7.242987, 112.726862',1,'Kegiatan dilakukan 3 minggu sekali atau 1 bulan sekali','\r',NULL,NULL,NULL),
	(183,60,'Melati','Jl. Kalibutuh Barat III, RT. 07 RW. 06',1,1,'7 orang','-7.256202, 112.722302',1,'Kegiatan dilakukan setiap hari sabtu','\r',NULL,NULL,NULL),
	(184,60,'Kampung Guyub','Jl. Demak Jaya Gg. X RT 14. RW 10',0,0,'','-',0,'','\r',NULL,NULL,NULL),
	(185,60,'Usaha Mandiri','Jl. Asembagus III/8',0,0,'','-',0,'','\r',NULL,NULL,NULL),
	(186,61,'Sekar Tanjung','Jl. Sumbermulyo 5',1,1,'50 orang','-7.246519, 112.726129',1,'','\r',NULL,NULL,NULL),
	(187,61,'Anggrek Makmur','Jl. Demak Timur V',1,1,'42 orang','-7.248123, 112.723388',1,'','\r',NULL,NULL,NULL),
	(188,61,'Maju Bersama','Jl. Margorukun Tengah',0,0,'','-',0,'','\r',NULL,NULL,NULL),
	(189,61,'Anggrek Makmur II','Jl. Demak Timur II',1,1,'','-7.249256, 112.722752',1,'Kegiatan dilakukan satu bulan sekali','\r',NULL,NULL,NULL),
	(190,61,'Sumber Makmur','Jl. Sumbermulyo 6',1,1,'40 orang','-7.246900, 112.725584',1,'Pengumpulan sampah dilakukan di depan rumah bu RT','\r',NULL,NULL,NULL),
	(191,61,'Maju Bersatu','Margodadi 3 no. 115',0,0,'','-',0,'','\r',NULL,NULL,NULL),
	(192,62,'Teratai','Maspati V (RT. 002, RW. 006)',1,1,'30 orang','-7.247602, 112.734793',1,'Kegiatan dilakukan dua minggu sekali','\r',NULL,NULL,NULL),
	(193,62,'Indah Sari','Maspati V (RT. 001, RW. 006)',0,0,'','-',0,'','\r',NULL,NULL,NULL),
	(194,62,'Jablay','Maspati V (RT. 003, RW. 006)',0,0,'','-',0,'','\r',NULL,NULL,NULL),
	(195,63,'kampoeng toewo','jl kawatan 10/25',1,1,'','-7.248205, 112.737692',1,'Aktif dari 2014.','Bu Muntoyah 081230601940\r',NULL,NULL,NULL),
	(196,64,'Jepara Makmur','Jl. Jepara RT 1 gg 5',0,0,'','-7.239074, 112.72458',0,'','\r',NULL,NULL,NULL),
	(197,64,'Kowandulling','Jl. Jepara RT 8 Gg 8',1,1,'','-7.237882, 112.724384',1,'Aktif dari 2017.','Bu Luluk 087855512012\r',NULL,NULL,NULL),
	(198,65,'mandiri jaya','Tambak arum gang 2 RT 1 RW 2',1,1,'','-7.248217, 112.75769',1,'Aktif dari 2016 tapi fluktuatif. 2018 baru jalan 2 bulan','\r',NULL,NULL,NULL),
	(199,65,'Kampung Hijau','simokerto',0,0,'','Done',0,'Tidak diketahui','\r',NULL,NULL,NULL),
	(200,65,'Guyub Sejahtera','Jl. Simokerto Tebasan no. 11 RW IV',1,1,'','-7.240966, 112.75823',1,'Aktif dari 2016. Tapi tutup tahun karena lebaran dan belum lanjut lagi','\r',NULL,NULL,NULL),
	(201,66,'Barokah Jaya','Jl. Sidoyoso Wetan',1,1,'','-7.237343, 112.763007',1,'Aktif dari 2014. (ada ipal+komposter juga)','\r',NULL,NULL,NULL),
	(202,66,'ERMA/ ERWE LIMA','Rusun Sumbo/ Jalan Sumbo',0,0,'','-7.234231, 112.747011',0,'Tidak aktif (sistem rusun dan penduduknya ruwet)','\r',NULL,NULL,NULL),
	(203,66,'Ikhlas Jaya','Kebun dalem 7/54 A',0,0,'','-7.231397, 112.746237',0,'Tidak aktif, berdiri tahun 2014 dg 23 nasabah, tapi penduduknya malas, pengepul merasa rugi karena sampahnya kurang banyak','\r',NULL,NULL,NULL),
	(204,67,'gencar mandiri','genteng candirejo RT 2 RW 8',1,1,'','-7.257984, 112.740109',1,'Aktif. Terakhir April 2018','\r',NULL,NULL,NULL),
	(205,67,'Serba Guna','Genteng Sidomukti',0,0,'','-7.257472, 112.740168',0,'Sudah tidak aktif. Diganti sistem simpan pinjam','\r',NULL,NULL,NULL),
	(206,68,'charismatik','ondomohen magersari V',1,1,'','-7.259603, 112.744683',1,'Aktif. Kampung green and clean, penduduk antusias tinggi','\r',NULL,NULL,NULL),
	(207,68,'Sekar Arum','Undaan Wetan 2/3',0,0,'','-7.253641, 112.745317',0,'Tidak aktif, dulu berdiri 2013, nasabah 50 orang','\r',NULL,NULL,NULL),
	(208,69,'FLAMBOYAN','Ngaglik gg V/5',0,0,'','-7.250344, 112.752322',0,'Tidak Aktif. Berdiri 2013/2014 berhenti tahun 2017','\r',NULL,NULL,NULL),
	(209,70,'del 6 mandiri','keputran panjunan 2/25 RT 2 RW 13',0,0,'','-7.276232, 112.741168',0,'Tidak aktif, berhenti tahun 2017 karena petugas tidak jujur','\r',NULL,NULL,NULL),
	(210,70,'aries daya anugrah','keputran kejambon 2/65',0,0,'','-7.275108, 112.739927',0,'Tidak aktif dari awal tahun 2018. berdiri sejak 2015, gerobak dicuri org jadi menghambat proses pengumpulan sampah','\r',NULL,NULL,NULL),
	(211,71,'Maju mapan','Peneleh gang 9 nomer 11',0,0,'','-7.252514, 112.739112',0,'Tidak diketahui','\r',NULL,NULL,NULL),
	(212,71,'senambung mulyo','lawang seketeng 6/5',1,1,'','-7.250285, 112.741724',0,'Aktif sampai sekarang. Berdiri 2016.','Bu Widi 0851005544164\r',NULL,NULL,NULL),
	(213,72,'Pioner','RT 12 RW 3 Kedungmangu 7',1,0,'30','-7.2251393, 112.7612599',1,'Tidak punya tempat penampungan','\r',NULL,NULL,NULL),
	(214,72,'Barokah','RT 17 RW 3',0,0,'-','-7.2261744, 112.7615653',0,'','\r',NULL,NULL,NULL),
	(215,72,'Best 9 OK','RT 12 RW 3',1,0,'6','-7.2254217, 112.7617165',1,'Tidak punya tempat penampungan','\r',NULL,NULL,NULL),
	(216,72,'Rempong Sejahter','RT 16 RW 3',0,0,'-','-7.226937, 112.760944',0,'','\r',NULL,NULL,NULL),
	(217,72,'Ceria Sidotopo','RT 8 RW 3 Kedungmangu Selatan 2/20',1,0,'30','-7.2274706, 112.7606789',1,'Tidak punya tempat penampungan','\r',NULL,NULL,NULL),
	(218,72,'Anugerah Suka Maju','RT 1 RW 3',0,0,'-','-7.224145, 112.761028',0,'','\r',NULL,NULL,NULL),
	(219,72,'Kartini Modern','RT 12 RW 3 Kedungmangu X',1,0,'Seluruh gang RT 12','-7.2256485, 112.7618339',1,'Tempat Pengumpulan RT 12, Tidak ada pembukuan','\r',NULL,NULL,NULL),
	(220,72,'Kemang','RT 12 RW 3',1,0,'12','-7.2252534, 112.7615737',1,'Tidak punya tempat penampungan','\r',NULL,NULL,NULL),
	(221,72,'Sido Makmur','Randu Agung 1, Sidotopo Wetan',0,0,'-','-7.2256485, 112.7644175',0,'','\r',NULL,NULL,NULL),
	(222,72,'Sekarsari','Platuk Donomulyo V/3 RW XIII',0,0,'-','-7.230061, 112.766381',0,'','\r',NULL,NULL,NULL),
	(223,72,'Sektoral Tak Nyana Barokah','Platuk Donomulyo V/3 RW XIII',0,0,'-','-7.230061, 112.766381',0,'','\r',NULL,NULL,NULL),
	(224,72,'Seroja','Kedungmangu 7a',1,0,'39','-7.2252959, 112.7613786',1,'Tidak punya tempat penampungan','\r',NULL,NULL,NULL),
	(225,72,'Arta Sampah Sejahtera','Pogot gang 8 No. 21',0,0,'-','-7.2334166, 112.7637959',0,'','\r',NULL,NULL,NULL),
	(226,73,'Mawar Merah','Jalan Tanah Merah Gang 2',1,0,'30','-7.2313375, 112.7677998',1,'Tidak ada data penimbangan karena penjualan dilakukan secara borongan','\r',NULL,NULL,NULL),
	(227,73,'Mentari','Rusun Tanah Merah 1 dan 2',0,0,'-','-7.2264727, 112.7696277',0,'','\r',NULL,NULL,NULL),
	(228,74,'Pandawa','RT 19 RW 1',0,0,'','-7.210833, 112.77422',0,'','\r',NULL,NULL,NULL),
	(229,74,'Asri','Tambak Wedi Gang Garuda RW 1',1,0,'30','-7.224118, 112.756604',1,'','\r',NULL,NULL,NULL),
	(230,75,'Mekar Sari II','Wonosari Lor 12 RT 8 RW 14',0,0,'','-7.219946, 112.752049',0,'','\r',NULL,NULL,NULL),
	(231,75,'Anggur','Wonosari Wetan Baru RW 7',1,0,'48','-7.216673, 112.754129',1,'Tidak memiliki tempat untuk memilah. Biasanya dilakukan dijalan. Penjualan borongan sehingga tidak ada lagi pembukuan','\r',NULL,NULL,NULL),
	(232,75,'Mekar Sari I','Bulak Sari RT 7 RW 6',1,0,'30','-7.214941, 112.75396',1,'Tidak memiliki tempat untuk memilah. Biasanya dilakukan dijalan. Penjualan borongan sehingga tidak ada lagi pembukuan','\r',NULL,NULL,NULL),
	(233,75,'Mekar Sari III','Wonosari Lor Baru X',0,0,'','-7.217278, 112.753141',0,'','\r',NULL,NULL,NULL),
	(234,75,'Mekar Sari II','Bulak Sari RT 3 RW 6',1,0,'30','-7.21556, 112.753993',1,'Tidak memiliki tempat untuk memilah. Biasanya dilakukan dijalan. Penjualan borongan sehingga tidak ada lagi pembukuan','\r',NULL,NULL,NULL),
	(235,75,'Mekar Kusuma','Wonokusumo XIV',0,0,'','-7.223573, 112.753446',0,'','\r',NULL,NULL,NULL),
	(236,75,'Tiger Ampel','Ampel Kesumba Pasar 17 RT 5 RW 2',1,0,'30','-7.231433, 112.743461',1,'Tidak ada data penimbangan karena dilakukan borongan, tidak memiliki tempat penampungan sampah. Biasanya dijalan','\r',NULL,NULL,NULL),
	(237,76,'Sejahtera','Jl. Komplek Hangtuah 1',1,0,'50','-7.222049, 112.743189',1,'Penjualan sampah dilakukan borongan sehingga tidak ada data penimbangan sampah','\r',NULL,NULL,NULL),
	(238,76,'Dolpin Rumah Pintar','Ujung Flat 1A',0,0,'','',0,'','\r',NULL,NULL,NULL),
	(239,76,'Nagapasa sejahtera','Jln. Nagapasa No.60',1,0,'30','-7.2149074, 112.759904',1,'Tidak ada data penimbangan karena dilakukan borongan','\r',NULL,NULL,NULL),
	(240,77,'Kamboja','Jl. Tenggumung Karya III/65',1,0,'23','-7.224741, 112.756253',1,'','\r',NULL,NULL,NULL),
	(241,78,'11 Makmur','Sidotopo Sekolahan I Nomer 33 B',1,0,'10','-7.229671, 112.753605',1,'','\r',NULL,NULL,NULL),
	(242,78,'Anggrek','Sidotopo Jaya gang 3A No. 20',1,0,'17','-7.23019, 112.75449',1,'','\r',NULL,NULL,NULL),
	(243,78,'Pucuk Merah','Sidotopo Sekolahan gang 2 No.4A Surabaya',1,0,'2','-7.229002, 112.751968',1,'Tidak memiliki tempat untuk memilah. Biasanya dilakukan dijalan','\r',NULL,NULL,NULL),
	(244,78,'Tiga Jaya','Sidotopo Sekolahan VII Nomer 6',1,0,'23','-7.229337, 112.752941',1,'','\r',NULL,NULL,NULL),
	(245,79,'Mawar','Jl. Bulak Cumpat Barat 4',1,0,'30','-7.2308772, 112.777035',1,'','\r',NULL,NULL,NULL),
	(246,79,'Hangtuah','Jl. Bulak Setro III-B',0,0,'','-7.232927, 112.782894',0,'','\r',NULL,NULL,NULL),
	(247,80,'Mandiri II','Kalimas baru 2 Lbr 15 R 2 RW 9',1,0,'','-7.21214, 112.735167',1,'Aktif dari tahun 2015. Masyarakat rajin mengumpulkan sampah','\r',NULL,NULL,NULL),
	(248,80,'Mandiri I','kalimas Baru 2 Lbr 26 RT 1 RW 9',1,0,'','-7.210663, 112.735072',1,'Fluktuatif, kadang aktif kadang gak, penimbangan 2018 blm terekap, pengurus keteteran, masyarkat kurang antusias','\r',NULL,NULL,NULL),
	(249,80,'Anper Mandiri','RT 4 RW VII',1,0,'','-7.214799, 112.729808',1,'Pengurus: karang taruna setempat. Nasabah dari RT 1-8','\r',NULL,NULL,NULL),
	(250,81,'BERSIH MANDIRI','tambak gringsing baru blok 3 gg 6 no n12',0,0,'','-7.2284426, 112.731135',0,'Tidak aktif. Berdiri tahun 2016. Pengurus karang taruna ganti jd tidak jalan bank sampahnya','\r',NULL,NULL,NULL),
	(251,82,'pusaka untung bersama','gadukan utara RT 10 RW 5',1,0,'','-7.230007, 112.713598',1,'Masih aktif namun ganti sistem mjd infaq sampah, sampah warga dikumpulkan sbg infaq utk kegiatan RT.','Pak Yanto 089654461292\r',NULL,NULL,NULL),
	(252,83,'chasablanka','krembangan barat RT 3 RW 11',1,0,'','-7.236678, 112.734168',1,'Aktif dari 2016. kegiatan daur ulang, antusias warga tinggi, hasil daur ulang dijual, sering menang lomba.','Bu Tini 081350275073\r',NULL,NULL,NULL),
	(253,83,'ibu kreatif jaya','Kemayoran baru buntu RT 1 RW 1',1,0,'60','-7.243566, 112.733004',1,'Aktif dari 2016.','\r',NULL,NULL,NULL),
	(254,84,'Merpati','Jl. Ikan Kerapu 4/27',1,0,'','-7.229523, 112.727533',1,'Aktif dari 2011 sampai sekarang. namun nasabah fluktuatif, masyarakat kadang semangat kadang tidak.','Bu Anis 082143479519\r',NULL,NULL,NULL),
	(255,84,'Tukusni Mandiri','Ikan Mungsing 7/ 23',0,0,'','Done',0,'','\r',NULL,NULL,NULL),
	(256,85,'Nusa Indah','dupak Bandarejo 2 / 10',0,0,'','Done',0,'',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `garbage_banks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table garbage_price_histories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `garbage_price_histories`;

CREATE TABLE `garbage_price_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `garbage_type_id` int(10) unsigned DEFAULT NULL,
  `garbage_bank_id` int(10) unsigned DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `price` int(11) unsigned DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table garbage_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `garbage_types`;

CREATE TABLE `garbage_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `garbage_types` WRITE;
/*!40000 ALTER TABLE `garbage_types` DISABLE KEYS */;

INSERT INTO `garbage_types` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'Clear PET bottle - not clean','2019-10-25 09:07:24',NULL,NULL),
	(2,'Clear PET bottle - clean','2019-10-25 09:07:28',NULL,NULL),
	(3,'Blue PET bottle - not clean','2019-10-25 09:07:26',NULL,NULL),
	(4,'Blue PET bottle - clean','2019-10-25 09:11:49',NULL,NULL),
	(5,'Colored PET bottle - clean','2019-10-25 09:11:49',NULL,NULL),
	(6,'Non-black plastic bucket','2019-10-25 09:11:49',NULL,NULL),
	(7,'Black plastic bucket','2019-10-25 09:11:49',NULL,NULL),
	(8,'Polystyrene jar','2019-10-25 09:11:49',NULL,NULL),
	(9,'Milk bottle','2019-10-25 09:11:49',NULL,NULL),
	(10,'Colored PET glass','2019-10-25 09:11:49',NULL,NULL),
	(11,'Bottle cap','2019-10-25 09:11:49',NULL,NULL),
	(12,'Gallon cap','2019-10-25 09:11:49',NULL,NULL),
	(13,'Gallon','2019-10-25 09:11:49',NULL,NULL),
	(14,'PET glass - not clean','2019-10-25 09:11:49',NULL,NULL),
	(15,'PET glass - clean','2019-10-25 09:11:49',NULL,NULL),
	(16,'Single use plastic packaging','2019-10-25 09:11:49',NULL,NULL),
	(17,'Single use plastic shopping bag','2019-10-25 09:11:49',NULL,NULL),
	(18,'Multilayer packaging','2019-10-25 09:11:49',NULL,NULL);

/*!40000 ALTER TABLE `garbage_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table villages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `villages`;

CREATE TABLE `villages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `district_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `district_id` (`district_id`),
  CONSTRAINT `villages_ibfk_1` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `villages` WRITE;
/*!40000 ALTER TABLE `villages` DISABLE KEYS */;

INSERT INTO `villages` (`id`, `district_id`, `name`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,'Dukuh Setro','2019-10-25 10:23:33',NULL,NULL),
	(2,2,'Medokan Semampir','2019-10-25 10:23:33',NULL,NULL),
	(3,2,'Keputih','2019-10-25 10:23:33',NULL,NULL),
	(4,2,'Menur Pumpungan','2019-10-25 10:23:33',NULL,NULL),
	(5,1,'Pacar Keling','2019-10-25 10:23:33',NULL,NULL),
	(6,1,'Kapas Madya Baru','2019-10-25 10:23:33',NULL,NULL),
	(7,1,'Ploso','2019-10-25 10:23:33',NULL,NULL),
	(8,1,'Gading','2019-10-25 10:23:33',NULL,NULL),
	(9,3,'Mojo','2019-10-25 10:23:33',NULL,NULL),
	(10,3,'Barata Jaya','2019-10-25 10:23:33',NULL,NULL),
	(11,3,'Gubeng','2019-10-25 10:23:33',NULL,NULL),
	(12,4,'Kutisari','2019-10-25 10:23:33',NULL,NULL),
	(13,4,'Panjang Jiwo','2019-10-25 10:23:33',NULL,NULL),
	(14,4,'Tenggilis Mejoyo','2019-10-25 10:23:33',NULL,NULL),
	(15,4,'Kendangsari','2019-10-25 10:23:33',NULL,NULL),
	(16,5,'Manyar Sabrangan','2019-10-25 10:23:33',NULL,NULL),
	(17,6,'Medokan Ayu','2019-10-25 10:23:33',NULL,NULL),
	(18,6,'Rungkut Kidul','2019-10-25 10:23:33',NULL,NULL),
	(19,6,'Kali Rungkut','2019-10-25 10:23:33',NULL,NULL),
	(20,6,'Kedung Baruk','2019-10-25 10:23:33',NULL,NULL),
	(21,7,'Rungkut Tengah','2019-10-25 10:23:33',NULL,NULL),
	(22,7,'Gunung Anyar Tambak','2019-10-25 10:23:33',NULL,NULL),
	(23,7,'Rungkut Menanggal','2019-10-25 10:23:33',NULL,NULL),
	(24,8,'Pakal','2019-10-25 10:23:33',NULL,NULL),
	(25,8,'Babat Jerawat','2019-10-25 10:23:33',NULL,NULL),
	(26,9,'Balongsari','2019-10-25 10:23:33',NULL,NULL),
	(27,9,'Manukan Kulon','2019-10-25 10:23:33',NULL,NULL),
	(28,9,'Manukan Wetan','2019-10-25 10:23:33',NULL,NULL),
	(29,9,'Karang Poh','2019-10-25 10:23:33',NULL,NULL),
	(30,9,'Tandes','2019-10-25 10:23:33',NULL,NULL),
	(31,9,'Banjar Sugihan','2019-10-25 10:23:33',NULL,NULL),
	(32,10,'Romokalisari','2019-10-25 10:23:33',NULL,NULL),
	(33,11,'Simomulyo','2019-10-25 10:23:33',NULL,NULL),
	(34,11,'Simomulyo Baru','2019-10-25 10:23:33',NULL,NULL),
	(35,11,'Tanjung Sari','2019-10-25 10:23:33',NULL,NULL),
	(36,12,'Lakarsantri','2019-10-25 10:23:33',NULL,NULL),
	(37,13,'Beringin','2019-10-25 10:23:33',NULL,NULL),
	(38,14,'Karah','2019-10-25 10:23:33',NULL,NULL),
	(39,14,'Jambangan','2019-10-25 10:23:33',NULL,NULL),
	(40,14,'Pagesangan','2019-10-25 10:23:33',NULL,NULL),
	(41,14,'Kebonsari','2019-10-25 10:23:33',NULL,NULL),
	(42,15,'Sawunggaling','2019-10-25 10:23:33',NULL,NULL),
	(43,15,'Ngagel Rejo','2019-10-25 10:23:33',NULL,NULL),
	(44,16,'Margorejo','2019-10-25 10:23:33',NULL,NULL),
	(45,17,'Kebraon','2019-10-25 10:23:33',NULL,NULL),
	(46,17,'Karang Pilang','2019-10-25 10:23:33',NULL,NULL),
	(47,17,'Kedurus','2019-10-25 10:23:33',NULL,NULL),
	(48,18,'Gayungan','2019-10-25 10:23:33',NULL,NULL),
	(49,19,'Sawahan','2019-10-25 10:23:33',NULL,NULL),
	(50,19,'Putat Jaya','2019-10-25 10:23:33',NULL,NULL),
	(51,20,'Wiyung','2019-10-25 10:23:33',NULL,NULL),
	(52,20,'Balas Kumprik','2019-10-25 10:23:33',NULL,NULL),
	(53,20,'Babatan','2019-10-25 10:23:33',NULL,NULL),
	(54,20,'Jajar Tunggal','2019-10-25 10:23:33',NULL,NULL),
	(55,21,'Dukuh Kupang','2019-10-25 10:23:33',NULL,NULL),
	(56,22,'Tegalsari','2019-10-25 10:23:33',NULL,NULL),
	(57,22,'Keputran','2019-10-25 10:23:33',NULL,NULL),
	(58,22,'Wonorejo','2019-10-25 10:23:33',NULL,NULL),
	(59,22,'Kedungdoro','2019-10-25 10:23:33',NULL,NULL),
	(60,23,'Tembok Dukuh','2019-10-25 10:23:33',NULL,NULL),
	(61,23,'Gundih','2019-10-25 10:23:33',NULL,NULL),
	(62,23,'Bubutan','2019-10-25 10:23:33',NULL,NULL),
	(63,23,'Alun-Alun Contong','2019-10-25 10:23:33',NULL,NULL),
	(64,23,'Jepara','2019-10-25 10:23:33',NULL,NULL),
	(65,24,'Tambak Rejo','2019-10-25 10:23:33',NULL,NULL),
	(66,24,'Simokerto','2019-10-25 10:23:33',NULL,NULL),
	(67,25,'Genteng','2019-10-25 10:23:33',NULL,NULL),
	(68,25,'Ketabang','2019-10-25 10:23:33',NULL,NULL),
	(69,25,'Kapasari','2019-10-25 10:23:33',NULL,NULL),
	(70,25,'Embong Kaliasin','2019-10-25 10:23:33',NULL,NULL),
	(71,25,'Peneleh','2019-10-25 10:23:33',NULL,NULL),
	(72,26,'Sidotopo Wetan','2019-10-25 10:23:33',NULL,NULL),
	(73,26,'Tanah Kali Kedinding','2019-10-25 10:23:33',NULL,NULL),
	(74,26,'Tambak Wedi','2019-10-25 10:23:33',NULL,NULL),
	(75,27,'Wonokusumo','2019-10-25 10:23:33',NULL,NULL),
	(76,27,'Ujung','2019-10-25 10:23:33',NULL,NULL),
	(77,27,'Pegirian','2019-10-25 10:23:33',NULL,NULL),
	(78,27,'Sidotopo','2019-10-25 10:23:33',NULL,NULL),
	(79,28,'Bulak','2019-10-25 10:23:33',NULL,NULL),
	(80,29,'Perak Utara','2019-10-25 10:23:33',NULL,NULL),
	(81,29,'Perak Timur','2019-10-25 10:23:33',NULL,NULL),
	(82,30,'Moro Krembangan','2019-10-25 10:23:33',NULL,NULL),
	(83,30,'Krembangan Selatan','2019-10-25 10:23:33',NULL,NULL),
	(84,30,'Perak Barat','2019-10-25 10:23:33',NULL,NULL),
	(85,30,'Dupak','2019-10-25 10:23:33',NULL,NULL);

/*!40000 ALTER TABLE `villages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table zones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `zones`;

CREATE TABLE `zones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `zones` WRITE;
/*!40000 ALTER TABLE `zones` DISABLE KEYS */;

INSERT INTO `zones` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'Surabaya Timur','2019-10-25 09:13:46',NULL,NULL),
	(2,'Surabaya Barat','2019-10-25 09:13:46',NULL,NULL),
	(3,'Surabaya Utara','2019-10-25 09:13:46',NULL,NULL),
	(4,'Surabaya Selatan','2019-10-25 09:13:46',NULL,NULL),
	(5,'Surabaya Pusat','2019-10-25 09:13:46',NULL,NULL);

/*!40000 ALTER TABLE `zones` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
