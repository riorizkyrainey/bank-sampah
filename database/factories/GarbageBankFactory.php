<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\GarbageBank;
use Faker\Generator as Faker;

$factory->define(GarbageBank::class, function (Faker $faker) {

    return [
        'village_id' => $faker->randomDigitNotNull,
        'name' => $faker->word,
        'address' => $faker->word,
        'active' => $faker->word,
        'photo' => $faker->text,
        'customer_count' => $faker->randomDigitNotNull,
        'latitude' => $faker->word,
        'longitude' => $faker->word,
        'pin' => $faker->word,
        'information' => $faker->text,
        'contact_person' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
