<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Village;
use Faker\Generator as Faker;

$factory->define(Village::class, function (Faker $faker) {

    return [
        'district_id' => $faker->randomDigitNotNull,
        'name' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
