<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\GarbagePriceHistory;
use Faker\Generator as Faker;

$factory->define(GarbagePriceHistory::class, function (Faker $faker) {

    return [
        'garbage_type_id' => $faker->randomDigitNotNull,
        'garbage_bank_id' => $faker->randomDigitNotNull,
        'date' => $faker->date('Y-m-d H:i:s'),
        'price' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
